<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Route::get('dashboard', function () {
    return view('admin.index');
})->name('index')->middleware('auth');



Route::get('/gender_setting', 'SettingController@indexGender')->name('gender.index');
Route::get('/gender_settings', 'SettingController@createGender')->name('gender.create');
Route::post('/gender_settings', 'SettingController@storeGender')->name('gender.store');
Route::get('/gender_setting/{gender_setting}','SettingController@editGender')->name('gender.edit')->where('id', '[0-9]+');
Route::put('/gender_setting/{gender_setting}','SettingController@updateGender')->name('gender.update')->where('id', '[0-9]+');
Route::delete('/gender_setting/{gender_setting}','SettingController@destroyGender')->name('gender.destroy')->where('id', '[0-9]+');

//Membership Grade Route
Route::get('/membership_setting', 'SettingController@indexMembership')->name('membership_grade.index');
Route::get('/membership_settings', 'SettingController@createMembership')->name('membership_grade.create');
Route::post('/membership_settings', 'SettingController@storeMembership')->name('membership_grade.store');
Route::get('/membership_setting/{membership_setting}','SettingController@editMembership')->name('membership_grade.edit')->where('id', '[0-9]+');
Route::put('/membership_setting/{membership_setting}','SettingController@updateMembership')->name('membership_grade.update')->where('id', '[0-9]+');
Route::delete('/membership_setting/{membership_setting}','SettingController@destroyMembership')->name('membership_grade.destroy')->where('id', '[0-9]+');

//Title Settings
Route::get('/title_setting', 'SettingController@indexTitle')->name('title.index');
Route::get('/title_settings', 'SettingController@createTitle')->name('title.create');
Route::post('/title_settings', 'SettingController@storeTitle')->name('title.store');
Route::get('/title_setting/{title_setting}','SettingController@editTitle')->name('title.edit')->where('id', '[0-9]+');
Route::put('/title_setting/{title_setting}','SettingController@updateTitle')->name('title.update')->where('id', '[0-9]+');
Route::delete('/title_setting/{title_setting}','SettingController@destroyTitle')->name('title.destroy')->where('id', '[0-9]+');

//Suffix Settings
Route::get('/suffixes', 'SettingController@indexSuffix')->name('suffix.index');
Route::get('/suffix_create', 'SettingController@createSuffix')->name('suffix.create');
Route::post('/suffixes', 'SettingController@storeSuffix')->name('suffix.store');
Route::get('/suffixes/{suffixes}','SettingController@editSuffix')->name('suffix.edit')->where('id', '[0-9]+');
Route::put('/suffixes/{suffixes}','SettingController@updateSuffix')->name('suffix.update')->where('id', '[0-9]+');
Route::delete('/suffixes/{suffixes}','SettingController@destroySuffix')->name('suffix.destroy')->where('id', '[0-9]+');

//Country Setting

Route::get('/countries', 'SettingController@indexCountry')->name('country.index');
Route::get('/country_create', 'SettingController@createCountry')->name('country.create');
Route::post('/country_settings', 'SettingController@storeCountry')->name('country.store');
Route::get('/countries/{countries}','SettingController@editCountry')->name('country.edit')->where('id', '[0-9]+');
Route::put('/country_setting/{country_setting}','SettingController@updateCountry')->name('country.update')->where('id', '[0-9]+');
Route::delete('/countries/{countries}','SettingController@destroyCountry')->name('country.destroy')->where('id', '[0-9]+');

//State Route
Route::get('/states', 'SettingController@indexState')->name('state.index');
Route::get('/state_create', 'SettingController@createState')->name('state.create');
Route::post('/state_settings', 'SettingController@storeState')->name('state.store');
Route::get('/states/{states}','SettingController@editState')->name('state.edit')->where('id', '[0-9]+');
Route::put('/state_setting/{state_setting}','SettingController@updateState')->name('state.update')->where('id', '[0-9]+');
Route::delete('/states/{states}','SettingController@destroyState')->name('state.destroy')->where('id', '[0-9]+');

//Sector Routing
Route::get('/sector', 'SettingController@indexSector')->name('sector.index');
Route::get('/sector_create', 'SettingController@createSector')->name('sector.create');
Route::post('/sector_settings', 'SettingController@storeSector')->name('sector.store');
Route::get('/sector/{sector}','SettingController@editSector')->name('sector.edit')->where('id', '[0-9]+');
Route::put('/sector_setting/{sector_setting}','SettingController@updateSector')->name('sector.update')->where('id', '[0-9]+');
Route::delete('/sectors/{sectors}','SettingController@destroySector')->name('sector.destroy')->where('id', '[0-9]+');

//User Status Routing
Route::get('/user_status', 'SettingController@indexUserStatus')->name('user_status.index');
Route::get('/user_status_create', 'SettingController@createUserStatus')->name('user_status.create');
Route::post('/user_status_settings', 'SettingController@storeUserStatus')->name('user_status.store');
Route::get('/user_status/{user_status}','SettingController@editUserStatus')->name('user_status.edit')->where('id', '[0-9]+');
Route::put('/user_status_settings/{user_status_settings}','SettingController@updateUserStatus')->name('user_status.update')->where('id', '[0-9]+');
Route::delete('/user_status/{user_status}','SettingController@destroyUserStatus')->name('user_status.destroy')->where('id', '[0-9]+');

// Qualification Routing
Route::get('/qualification', 'SettingController@indexQualification')->name('qualification.index');
Route::get('/qualification_create', 'SettingController@createQualification')->name('qualification.create');
Route::post('/qualification_settings', 'SettingController@storeQualification')->name('qualification.store');
Route::get('/qualification/{qualification}','SettingController@editQualification')->name('qualification.edit')->where('id', '[0-9]+');
Route::put('/qualification_settings/{qualification_settings}','SettingController@updateQualification')->name('qualification.update')->where('id', '[0-9]+');
Route::delete('/qualification/{qualification}','SettingController@destroyQualification')->name('qualification.destroy')->where('id', '[0-9]+');

// Interest Area Routing
Route::get('/interest_area', 'SettingController@indexInterestArea')->name('interest_area.index');
Route::get('/interest_area_create', 'SettingController@createInterestArea')->name('interest_area.create');
Route::post('/interest_area_settings', 'SettingController@storeInterestArea')->name('interest_area.store');
Route::get('/interest_area/{interest_area}','SettingController@editInterestArea')->name('interest_area.edit')->where('id', '[0-9]+');
Route::put('/interest_area/{interest_area}','SettingController@updateInterestArea')->name('interest_area.update')->where('id', '[0-9]+');
Route::delete('/interest_area/{interest_area}','SettingController@destroyInterestArea')->name('interest_area.destroy')->where('id', '[0-9]+');

// Technical Focus Routing
Route::get('/technical_focus', 'SettingController@indexTechnicalFocus')->name('technical_focus.index');
Route::get('/technical_focus_create', 'SettingController@createTechnicalFocus')->name('technical_focus.create');
Route::post('/technical_focus_settings', 'SettingController@storeTechnicalFocus')->name('technical_focus.store');
Route::get('/technical_focus/{technical_focus}','SettingController@editTechnicalFocus')->name('technical_focus.edit')->where('id', '[0-9]+');
Route::put('/technical_focus/{technical_focus}','SettingController@updateTechnicalFocus')->name('technical_focus.update')->where('id', '[0-9]+');
Route::delete('/technical_focus/{technical_focus}','SettingController@destroyTechnicalFocus')->name('technical_focus.destroy')->where('id', '[0-9]+');

// Educational Level Routing
Route::get('/educational_level', 'SettingController@indexEducationalLevel')->name('educational_level.index');
Route::get('/educational_level_create', 'SettingController@createEducationalLevel')->name('educational_level.create');
Route::post('/educational_level_settings', 'SettingController@storeEducationalLevel')->name('educational_level.store');
Route::get('/educational_level/{educational_level}','SettingController@editEducationalLevel')->name('educational_level.edit')->where('id', '[0-9]+');
Route::put('/educational_level/{educational_level}','SettingController@updateEducationalLevel')->name('educational_level.update')->where('id', '[0-9]+');
Route::delete('/educational_level/{educational_level}','SettingController@destroyEducationalLevel')->name('educational_level.destroy')->where('id', '[0-9]+');

// Educational Level Routing
Route::get('/interest_level', 'SettingController@indexInterestLevel')->name('interest_level.index');
Route::get('/interest_level_create', 'SettingController@createInterestLevel')->name('interest_level.create');
Route::post('/educational_level_settings', 'SettingController@storeInterestLevel')->name('interest_level.store');
Route::get('/interest_level/{interest_level}','SettingController@editInterestLevel')->name('interest_level.edit')->where('id', '[0-9]+');
Route::put('/interest_level/{interest_level}','SettingController@updateInterestLevel')->name('interest_level.update')->where('id', '[0-9]+');
Route::delete('/interest_level/{interest_level}','SettingController@destroyInterestLevel')->name('interest_level.destroy')->where('id', '[0-9]+');

// Security Question Routing
Route::get('/security_question', 'SettingController@indexSecurityQuestion')->name('security_question.index');
Route::get('/security_question_create', 'SettingController@createSecurityQuestion')->name('security_question.create');
Route::post('/security_question_settings', 'SettingController@storeSecurityQuestion')->name('security_question.store');
Route::get('/security_question/{security_question}','SettingController@editSecurityQuestion')->name('security_question.edit')->where('id', '[0-9]+');
Route::put('/security_question/{security_question}','SettingController@updateSecurityQuestion')->name('security_question.update')->where('id', '[0-9]+');
Route::delete('/security_question/{security_question}','SettingController@destroySecurityQuestion')->name('security_question.destroy')->where('id', '[0-9]+');

// Security Question Routing
Route::resource('/member_info', 'MemberController');


Route::get('/update_info', 'UserController@create')->name('update_info.create')->name('home')->middleware('auth');
Route::post('/update_info', 'UserController@store')->name('update_info.store');
Route::get('/update_info/{update_info}','UserController@edit')->name('update_info.edit')->where('id', '[0-9]+');
Route::put('/update_info/{update_info}','UserController@update')->name('update_info.update')->where('id', '[0-9]+');
Route::delete('/update_info/{update_info}','UserController@destroy')->name('update_info.destroy')->where('id', '[0-9]+');




//This section belongs to the student side
//Route::get('/', function () {
//    return view('student.update-info');
//})->middleware('auth')->name('welcome');


Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('welcome', function () {
    return view('welcome');
})->name('wait');

Route::get('test', function () {
    return view('admin.test');
});
