<?php

namespace App\Http\Controllers;

use App\Country;
use App\EducationalLevel;
use App\Gender;
use App\InterestArea;
use App\InterestLevel;
use App\MembershipGrade;
use App\Qualification;
use App\Sector;
use App\SecurityQuestion;
use App\State;
use App\Suffix;
use App\TechnicalFocus;
use App\Title;
use App\UserStatus;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;
use Ramsey\Uuid\Codec\TimestampLastCombCodec;

class SettingController extends Controller
{
    //

    public function indexGender()
    {
        $gender = Gender::all();
        return view('admin.gender-list', compact('gender'));

    }

    public function createGender()
    {

        $gender = new Gender();
        return view('admin.gender_add', compact('gender'));
    }

    public function storeGender(Request $request)
    {

        $data = $this->getData($request);

        Gender::create($data);
        return redirect()->route('gender.index')
            ->with('success_message','Gender created successfully.');
    }

    protected function getData(Request $request){
        $rules = [
            'gender' => 'nullable',
        ];
        return $request->validate($rules);
    }
    public function editGender($id)
    {
        $gender = Gender::findOrFail($id);
        return view('admin.gender_edit',compact('gender'));
    }

    public function updateGender(Request $request, $id)
    {
        $gender =  Gender::findOrfail($id);
        $request->validate([
            'gender' => 'nullable',
        ]);

       $gender->update($request->all());
        return redirect()->route('gender.index')
            ->with('success','Gender updated successfully');
    }
    public function destroyGender($id)
    {
        $gender = Gender::findOrFail($id);
        $gender->delete();
        return redirect()->back()
            ->with('success_destroy','Gender deleted successfully');
    }



//    Membership Controller

    public function indexMembership()
    {
        $membership = MembershipGrade::all();
        return view('admin.membership-grade-list', compact('membership'));

    }

    public function createMembership()
    {

        $membership = new MembershipGrade();
        return view('admin.membership-grade-add', compact('membership'));
    }

    public function storeMembership(Request $request)
    {

        $data = $this->getMembership($request);

        MembershipGrade::create($data);
        return redirect()->route('membership_grade.index')
            ->with('success_message','Membership Grade created successfully.');
    }

    protected function getMembership(Request $request){
        $rules = [
            'membership_grade' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editMembership($id)
    {
        $membership = MembershipGrade::findOrFail($id);
        return view('admin.membership-grade-edit',compact('membership'));
    }

    public function updateMembership(Request $request, $id)
    {
        $membership =  MembershipGrade::findOrfail($id);
        $request->validate([
            'membership_grade' => 'nullable',
        ]);

        $membership->update($request->all());
        return redirect()->route('membership_grade.index')
            ->with('success','Membership Grade updated successfully');
    }
    public function destroyMembership($id)
    {
        $membership = MembershipGrade::findOrFail($id);
        $membership->delete();
        return redirect()->back()
            ->with('success_destroy','Membership Grade deleted successfully');
    }


    //    Title Controller

    public function indexTitle()
    {
        $title = Title::all();
        return view('admin.title-list', compact('title'));

    }
    public function createTitle()
    {

        $title = new Title();
        return view('admin.title-add', compact('title'));
    }

    public function storeTitle(Request $request)
    {

        $data = $this->getTitle($request);

        Title::create($data);
        return redirect()->route('title.index')
            ->with('success_message','Title created successfully.');
    }

    protected function getTitle(Request $request){
        $rules = [
            'title' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editTitle($id)
    {
        $title = Title::findOrFail($id);
        return view('admin.title-edit',compact('title'));
    }

    public function updateTitle(Request $request, $id)
    {
        $title =  Title::findOrfail($id);
        $request->validate([
            'title' => 'nullable',
        ]);

        $title->update($request->all());
        return redirect()->route('title.index')
            ->with('success','Title updated successfully');
    }
    public function destroyTitle($id)
    {
        $title = Title::findOrFail($id);
        $title->delete();
        return redirect()->back()
            ->with('success_destroy','Title deleted successfully');
    }



    //    Suffix Controller

    public function indexSuffix()
    {
        $suffix = Suffix::all();
        return view('admin.suffix-list', compact('suffix'));

    }

    public function createSuffix()
    {

        $suffix = new Suffix();
        return view('admin.suffix-add', compact('suffix'));
    }

    public function storeSuffix(Request $request)
    {

        $data = $this->getSuffix($request);

        Suffix::create($data);
        return redirect()->route('suffix.index')
            ->with('success_message','Suffix created successfully.');
    }

    protected function getSuffix(Request $request){
        $rules = [
            'suffix' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editSuffix($id)
    {
        $suffix = Suffix::findOrFail($id);
        return view('admin.suffix-edit',compact('suffix'));
    }

    public function updateSuffix(Request $request, $id)
    {
        $suffix =  Suffix::findOrfail($id);
        $request->validate([
            'suffix' => 'nullable',
        ]);

        $suffix->update($request->all());
        return redirect()->route('suffix.index')
            ->with('success','Suffix updated successfully');
    }
    public function destroySuffix($id)
    {
        $suffix = Suffix::findOrFail($id);
        $suffix->delete();
        return redirect()->back()
            ->with('success_destroy','Suffix deleted successfully');
    }




    //    Country Controller

    public function indexCountry()
    {
        $country = Country::all();
        return view('admin.country-list', compact('country'));

    }

    public function createCountry()
    {

        $country = new Country();
        return view('admin.country-add', compact('country'));
    }

    public function storeCountry(Request $request)
    {

        $data = $this->getCountry($request);

        Country::create($data);
        return redirect()->route('country.index')
            ->with('success_message','Country created successfully.');
    }

    protected function getCountry(Request $request){
        $rules = [
            'country' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editCountry($id)
    {
        $country = Country::findOrFail($id);
        return view('admin.country-edit',compact('country'));
    }

    public function updateCountry(Request $request, $id)
    {
        $country =  Country::findOrfail($id);
        $request->validate([
            'country' => 'nullable',
        ]);

        $country->update($request->all());
        return redirect()->route('country.index')
            ->with('success','Country updated successfully');
    }
    public function destroyCountry($id)
    {
        $country = Country::findOrFail($id);
        $country->delete();
        return redirect()->back()
            ->with('success_destroy','Country deleted successfully');
    }


//     State Controller

    public function indexState()
    {
        $state = State::all();
        return view('admin.state-list', compact('state'));

    }

    public function createState()
    {

        $state = new State();
        $country = Country::select('id', 'country')->get();
        return view('admin.state-add', compact('state', 'country'));
    }

    public function storeState(Request $request)
    {

        $data = $this->getState($request);
        State::create($data);
        return redirect()->route('state.index')
            ->with('success_message','State created successfully.');
    }

    protected function getState(Request $request){
        $rules = [
            'state' => 'required',
            'country_id' => 'required'
        ];
        return $request->validate($rules);
    }

    public function editState($id)
    {
        $state = State::findOrFail($id);
        $countries = Country::all();
//        $country = Country::select('id', 'country')->get();
        return view('admin.state-edit',compact('state', 'countries'));
    }

    public function updateState(Request $request, $id)
    {
        $state =  State::findOrfail($id);

        $request->validate([
            'country_id' => 'required',
            'state' => 'required',
        ]);

        $state->update($request->all());
        return redirect()->route('state.index')
            ->with('success','State updated successfully');
    }
    public function destroyState($id)
    {
        $state = State::findOrFail($id);
        $state->delete();
        return redirect()->back()
            ->with('success_destroy','State deleted successfully');
    }

    //     Sector Controller

    public function indexSector()
    {
        $sectors = Sector::all();
        return view('admin.sector-list', compact('sectors'));

    }

    public function createSector()
    {

        $sector = new Sector();
        return view('admin.sector-add', compact('sector'));
    }


    public function storeSector(Request $request)
    {

        $data = $this->getSector($request);
        Sector::create($data);
        return redirect()->route('sector.index')
            ->with('success_message','Sector created successfully.');
    }

    protected function getSector(Request $request){
        $rules = [
            'sector' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editSector($id)
    {
        $sector = Sector::findOrFail($id);

        return view('admin.sector-edit',compact('sector'));
    }

    public function updateSector(Request $request, $id)
    {
        $sector = Sector::findOrfail($id);

        $request->validate([
            'sector' => 'nullable',
        ]);

        $sector->update($request->all());
        return redirect()->route('sector.index')
            ->with('success','Sector updated successfully');
    }
    public function destroySector($id)
    {
        $sector = Sector::findOrFail($id);
        $sector->delete();
        return redirect()->back()
            ->with('success_destroy','User Status deleted successfully');
    }



    //     User Status Controller

    public function indexUserStatus()
    {
        $user_statuses = UserStatus::all();
        return view('admin.user-status-list', compact('user_statuses'));

    }

    public function createUserStatus()
    {
        $user_statuses = new UserStatus();
        return view('admin.user-status-add', compact('user_statuses'));
    }


    public function storeUserStatus(Request $request)
    {
        $data = $this->getUserStatus($request);
        UserStatus::create($data);
        return redirect()->route('user_status.index')
            ->with('success_message','User Status created successfully.');
    }

    protected function getUserStatus(Request $request){
        $rules = [
            'user_status' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editUserStatus($id)
    {
        $user_statuses = UserStatus::findOrFail($id);

        return view('admin.user-status-edit',compact('user_statuses'));
    }

    public function updateUserStatus(Request $request, $id)
    {
        $user_statuses = UserStatus::findOrfail($id);

        $request->validate([
            'user_status' => 'nullable',
        ]);

        $user_statuses->update($request->all());
        return redirect()->route('user_status.index')
            ->with('success','User Status updated successfully');
    }
    public function destroyUserStatus($id)
    {
        $user_statuses = UserStatus::findOrFail($id);
        $user_statuses->delete();
        return redirect()->back()
            ->with('success_destroy','User Status deleted successfully');
    }


    //     Qualification Controller

    public function indexQualification()
    {
        $qualification = Qualification::all();
        return view('admin.qualification-list', compact('qualification'));

    }

    public function createQualification()
    {
        $qualification = new Qualification();
        return view('admin.qualification-add', compact('qualification'));
    }


    public function storeQualification(Request $request)
    {
        $data = $this->getQualification($request);
        Qualification::create($data);
        return redirect()->route('qualification.index')
            ->with('success_message','Qualification created successfully.');
    }

    protected function getQualification(Request $request){
        $rules = [
            'qualifications' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editQualification($id)
    {
        $qualification = Qualification::findOrFail($id);

        return view('admin.qualification-edit',compact('qualification'));
    }

    public function updateQualification(Request $request, $id)
    {
        $qualification = Qualification::findOrfail($id);

        $request->validate([
            'qualifications' => 'nullable',
        ]);

        $qualification->update($request->all());
        return redirect()->route('qualification.index')
            ->with('success','Qualification updated successfully');
    }
    public function destroyQualification($id)
    {
        $qualification = Qualification::findOrFail($id);
        $qualification->delete();
        return redirect()->back()
            ->with('success_destroy','Qualification deleted successfully');
    }


    //     Interest Area Controller

    public function indexInterestArea()
    {
        $interest_area = InterestArea::all();
        return view('admin.interest-area-list', compact('interest_area'));

    }

    public function createInterestArea()
    {
        $interest_area = new InterestArea();
        return view('admin.interest-area-add', compact('interest_area'));
    }


    public function storeInterestArea(Request $request)
    {
        $data = $this->getInterestArea($request);
        InterestArea::create($data);
        return redirect()->route('interest_area.index')
            ->with('success_message','Interest Area created successfully.');
    }

    protected function getInterestArea(Request $request){
        $rules = [
            'interest_area' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editInterestArea($id)
    {
        $interest_area = InterestArea::findOrFail($id);

        return view('admin.interest-area-edit',compact('interest_area'));
    }

    public function updateInterestArea(Request $request, $id)
    {
        $interest_area = InterestArea::findOrfail($id);


        $request->validate([
            'interest_area' => 'nullable',
        ]);

        $interest_area->update($request->all());
        return redirect()->route('interest_area.index')
            ->with('success','Interest Area updated successfully');
    }
    public function destroyInterestArea($id)
    {
        $interest_area = InterestArea::findOrFail($id);
        $interest_area->delete();
        return redirect()->back()
            ->with('success_destroy','Interest Area deleted successfully');
    }

    //  Technical Focus Controller

    public function indexTechnicalFocus()
    {
        $technical_focus = TechnicalFocus::all();
        return view('admin.technical-focus-list', compact('technical_focus'));

    }

    public function createTechnicalFocus()
    {
        $technical_focus = new TechnicalFocus();
        return view('admin.technical-focus-add', compact('technical_focus'));
    }


    public function storeTechnicalFocus(Request $request)
    {
        $data = $this->getTechnicalFocus($request);
        TechnicalFocus::create($data);
        return redirect()->route('technical_focus.index')
            ->with('success_message','Technical Focus created successfully.');
    }

    protected function getTechnicalFocus(Request $request){
        $rules = [
            'technical_focus' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editTechnicalFocus($id)
    {
        $technical_focus = TechnicalFocus::findOrFail($id);

        return view('admin.technical-focus-edit',compact('technical_focus'));
    }

    public function updateTechnicalFocus(Request $request, $id)
    {
        $technical_focus = TechnicalFocus::findOrfail($id);


        $request->validate([
            'technical_focus' => 'nullable',
        ]);

        $technical_focus->update($request->all());
        return redirect()->route('technical_focus.index')
            ->with('success','Technical Focus updated successfully');
    }
    public function destroyTechnicalFocus($id)
    {
        $technical_focus = TechnicalFocus::findOrFail($id);
        $technical_focus->delete();
        return redirect()->back()
            ->with('success_destroy','Technical Focus Area deleted successfully');
    }

    //  Educational Level Controller

    public function indexEducationalLevel()
    {
        $educational_level = EducationalLevel::all();
        return view('admin.educational-level-list', compact('educational_level'));

    }

    public function createEducationalLevel()
    {
        $educational_level = new EducationalLevel();
        return view('admin.educational-level-add', compact('educational_level'));
    }


    public function storeEducationalLevel(Request $request)
    {
        $data = $this->getEducationalLevel($request);
        EducationalLevel::create($data);
        return redirect()->route('educational_level.index')
            ->with('success_message','Educational Level created successfully.');
    }

    protected function getEducationalLevel(Request $request){
        $rules = [
            'educational_level' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editEducationalLevel($id)
    {
        $educational_level = EducationalLevel::findOrFail($id);

        return view('admin.educational-level-edit',compact('educational_level'));
    }

    public function updateEducationalLevel(Request $request, $id)
    {
        $educational_level = EducationalLevel::findOrfail($id);


        $request->validate([
            'educational_level' => 'nullable',
        ]);

        $educational_level->update($request->all());
        return redirect()->route('educational_level.index')
            ->with('success','Educational Level updated successfully');
    }
    public function destroyEducationalLevel($id)
    {
        $educational_level = EducationalLevel::findOrFail($id);
        $educational_level->delete();
        return redirect()->back()
            ->with('success_destroy','Educational Level Area deleted successfully');
    }

    //  Interest Level Controller

    public function indexInterestLevel()
    {
        $interest_level = InterestLevel::all();
        return view('admin.interest-level-list', compact('interest_level'));

    }

    public function createInterestLevel()
    {
        $interest_level = new InterestLevel();
        return view('admin.interest-level-add', compact('interest_level'));
    }


    public function storeInterestLevel(Request $request)
    {
        $data = $this->getInterestLevel($request);
        InterestLevel::create($data);
        return redirect()->route('interest_level.index')
            ->with('success_message','Interest Level created successfully.');
    }

    protected function getInterestLevel(Request $request){
        $rules = [
            'interest_level' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editInterestLevel($id)
    {
        $interest_level = InterestLevel::findOrFail($id);

        return view('admin.interest-level-edit',compact('interest_level'));
    }

    public function updateInterestLevel(Request $request, $id)
    {
        $interest_level = InterestLevel::findOrfail($id);


        $request->validate([
            'interest_level' => 'nullable',
        ]);

        $interest_level->update($request->all());
        return redirect()->route('interest_level.index')
            ->with('success','Interest Level updated successfully');
    }
    public function destroyInterestLevel($id)
    {
        $interest_level = InterestLevel::findOrFail($id);
        $interest_level->delete();
        return redirect()->back()
            ->with('success_destroy','Interest Level Area deleted successfully');
    }


    //  Security Question Controller

    public function indexSecurityQuestion()
    {
        $security_question = SecurityQuestion::all();
        return view('admin.security-question-list', compact('security_question'));

    }

    public function createSecurityQuestion()
    {
        $security_question = new SecurityQuestion();
        return view('admin.security-question-add', compact('security_question'));
    }


    public function storeSecurityQuestion(Request $request)
    {
        $data = $this->getSecurityQuestion($request);
        SecurityQuestion::create($data);
        return redirect()->route('security_question.index')
            ->with('success_message','Security Question created successfully.');
    }

    protected function getSecurityQuestion(Request $request){
        $rules = [
            'security_question' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function editSecurityQuestion($id)
    {
        $security_question = SecurityQuestion::findOrFail($id);

        return view('admin.security-question-edit',compact('security_question'));
    }

    public function updateSecurityQuestion(Request $request, $id)
    {
        $security_question = SecurityQuestion::findOrfail($id);


        $request->validate([
            'security_question' => 'nullable',
        ]);

        $security_question->update($request->all());
        return redirect()->route('security_question.index')
            ->with('success','Security Question updated successfully');
    }
    public function destroySecurityQuestion($id)
    {
        $security_question = SecurityQuestion::findOrFail($id);
        $security_question->delete();
        return redirect()->back()
            ->with('success_destroy','Security Question Area deleted successfully');
    }



}
