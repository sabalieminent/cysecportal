<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    //

    public function index()
    {
//        return auth()->user();
        $user_data = User::all();
        return view('admin.member-list', compact('user_data'));
    }

    public function show($id)
    {
        $user_data = User::findBySlug($id);
        return view('admin.membership-view.show')->with('user_data', $user_data);
    }

}
