<?php

namespace App\Http\Controllers;

use App\Country;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
//    public function index()
//    {
//        $user_data = User::all();
//        return view('admin.member-list', compact('user_data'));
//    }

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }


    public function create()
    {
        $user_data = new User();
        $country = Country::all();
        return view('student.update-info', compact('user_data', 'country'));
    }


    public function store(Request $request)
    {

        $data = $this->getData($request);
        auth()->user()->update($data);
        return redirect()->route('wait')
            ->with('success_message','User Data created successfully.');
    }

    protected function getData(Request $request){
        $rules = [
            'first_name' => 'nullable',
            'last_name' => 'nullable',
            'email' => 'required|email',
            'gender' => 'nullable',
            'phone' => 'required',
            'profile_image' => 'nullable',
            'address' => 'nullable',
            'bio' => 'nullable',
            'post_code' => 'nullable',
            'city' => 'nullable',
            'state' => 'nullable',
            'country' => 'nullable',
        ];
        return $request->validate($rules);
    }

    public function edit($id)
    {
        $user_data = User::findOrFail($id);
        $country = Country::all();
        return view('student.update-info',compact('user_data', 'country'));
    }

    public function update(Request $request, $id)
    {
        $user_data = User::findOrfail($id);


        $request->validate([
            'first_name' => 'nullable',
            'last_name' => 'nullable',
            'email' => 'required|email',
            'gender' => 'nullable',
            'phone' => 'required',
            'profile_image' => 'nullable',
            'address' => 'nullable',
            'bio' => 'nullable',
            'post_code' => 'nullable',
            'city' => 'nullable',
            'state' => 'nullable',
            'country' => 'nullable',
        ]);

        $user_data->update($request->all());
        return redirect()->route('member_info.index')
            ->with('success','Security Question updated successfully');
    }
    public function destroy($id)
    {
        $security_question = User::findOrFail($id);
        $security_question->delete();
        return redirect()->back()
            ->with('success_destroy','Security Question Area deleted successfully');
    }






}
