<?php

namespace App\Http\Controllers;

use App\Gender;
use App\Settings;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexGender()
    {
        //
        $gender = Gender::all();
        return view('admin.gender-list', compact('gender'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createGender()
    {
        //
        $gender = new Gender();
        return view('admin.gender_add', compact('gender'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $this->getData($request);

        Settings::create($data);
        return redirect()->route('settings.index')
            ->with('success_message','Gender created successfully.');
    }

    protected function getData(Request $request){
        $rules = [
            'gender' => 'nullable',
            'country' => 'nullable',
            'membership_grade' => 'nullable',
            'title' => 'nullable',
            'state' => 'nullable',
            'sector' =>   'nullable',
            'qualification' => 'nullable',
            'interest_area' => 'nullable',
            'interest_level' => 'nullable',
            'educational_level' => 'nullable',
            'technical_focus' => 'nullable',
            'employee_type' => 'nullable',
            'security_question' => 'nullable',
            'user_status' => 'nullable',
        ];
        return $request->validate($rules);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Responsesettings
     */
    public function show(Settings $settings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function edit(Settings $settings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Settings $settings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Settings $settings)
    {
        //
    }
}
