<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembershipGrade extends Model
{
    //
    protected $table = 'membership_grade';
    protected $guarded = [];
}
