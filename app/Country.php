<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table = 'country';
    protected $guarded = [];

    public function state(){
        return $this->hasMany(State::class);

    }


}
