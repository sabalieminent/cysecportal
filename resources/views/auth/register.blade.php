@include ("student.header-login")

		<!-- begin:: Page -->
		<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
					<div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
						<div class="kt-login__wrapper">
							<div class="kt-login__container">
								<div class="kt-login__body">
									<div class="kt-login__logo">
										<a href="#">
											<img src="student_assets/media/company-logos/logo-2.png">
										</a>
									</div>
                                    <div class="">
                                        <div class="kt-login__head">
                                            <h3 class="kt-login__title">Sign Up</h3>
                                            <div class="kt-login__desc">Enter your details to create your account:</div>
                                        </div>
                                        <div class="kt-login__form">
                                            <form method="POST" action="{{ route('register') }}">
                                                @csrf

                                                <div class="form-group">

                                                        <input id="first_name" type="text" placeholder="First Name" class="form-control" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>

                                                        @error('first_name')
                                                        <span class="invalid-feedback alert alert-dismissible" role="alert">
                                                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                </div>

                                                <div class="form-group">

                                                    <input id="last_name" type="text" placeholder="Last Name" class="form-control" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

                                                    @error('last_name')
                                                    <span class="invalid-feedback alert alert-dismissible" role="alert">
                                                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                        <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email">

                                                        @error('email')
                                                        <span class="invalid-feedback alert alert-dismissible" role="alert">
                                                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                </div>

                                                <div class="form-group">

                                                        <input id="password" type="password" placeholder="Password" class="form-control form-control-last" name="password" required autocomplete="new-password">

                                                        @error('password')
                                                        <span class="invalid-feedback alert alert-dismissible" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror

                                                </div>

                                                <div class="form-group">
                                                        <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                                                </div>

                                                <div class="kt-login__extra">
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="agree" required> I Agree the <a href="#">terms and conditions</a>.
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="kt-login__actions">
                                                    <button type="submit" class="btn btn-brand btn-pill btn-elevate">
                                                        {{ __('Register') }}
                                                    </button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>


									<div class="kt-login__forgot">
										<div class="kt-login__head">
											<h3 class="kt-login__title">Forgotten Password ?</h3>
											<div class="kt-login__desc">Enter your email to reset your password:</div>
										</div>
										<div class="kt-login__form">
											<form class="kt-form" action="">
												<div class="form-group">
													<input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
												</div>
												<div class="kt-login__actions">
													<button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">Request</button>
													<button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">Cancel</button>
												</div>
											</form>
										</div>
									</div>

								</div>
							</div>
							<div class="kt-login__account">
								<span class="kt-login__account-msg">
									Don't have an account yet ?
								</span>&nbsp;&nbsp;
								<a href="{{ route('register') }}" class="kt-login__account-link">Sign Up!</a>
							</div>
						</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url(student_assets/media/bg/bg-4.jpg);">
						<div class="kt-login__section">
							<div class="kt-login__block">
								<h3 class="kt-login__title">Join Our Community</h3>
								<div class="kt-login__desc">
									Lorem ipsum dolor sit amet, coectetuer adipiscing
									<br>elit sed diam nonummy et nibh euismod
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->

@include ("student.footer-login")


		<!--begin::Page Scripts(used by this page) -->
		<script src="student_assets/js/pages/custom/login/login-general.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
