@include("admin.header")
						<!-- begin:: Content Head -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-container  kt-container--fluid ">
								<div class="kt-subheader__main">
									<h3 class="kt-subheader__title">
										View Contact
									</h3>

								</div>
								<div class="kt-subheader__toolbar">
									<a href="#" class="btn btn-success btn-bold">
										Back </a>
								</div>
							</div>
						</div>

						<!-- end:: Content Head -->

						<!-- begin:: Content -->
						<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

							<!--Begin:: Portlet-->
							<div class="kt-portlet">
								<div class="kt-portlet__body">
									<div class="kt-widget kt-widget--user-profile-3">
										<div class="kt-widget__top">
											<div class="kt-widget__media">
												<img src="assets/media/users/100_12.jpg" alt="image">
											</div>
											<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-bolder kt-font-light kt-hidden">
												JM
											</div>
											<div class="kt-widget__content">
												<div class="kt-widget__head">
													<div class="kt-widget__user">
														<a href="#" class="kt-widget__username" style="font-weight: bold">
															Ositadimma Tochukwu Michael
														</a>
														<!--<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success">Customer</span>
														<div class="dropdown dropdown-inline kt-margin-l-5" data-toggle="kt-tooltip-" title="Change label" data-placement="right">
															<a href="#" class="btn btn-clean btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="fa fa-caret-down"></i>
															</a>
															<div class="dropdown-menu dropdown-menu-md dropdown-menu-fit dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__head">
																		Choose label:
																		<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
																	</li>
																	<li class="kt-nav__separator">
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="1">
																			<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--bold">Customer</span></span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="2">
																			<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--bold">Partner</span></span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="3">
																			<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--bold">Supplier</span></span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="4">
																			<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-info kt-badge--inline kt-badge--lg kt-badge--bold">On Hold</span></span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="4">
																			<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-brand kt-badge--inline kt-badge--lg kt-badge--bold">Staff</span></span>
																		</a>
																	</li>
																	<li class="kt-nav__separator">
																	</li>
																	<li class="kt-nav__foot">
																		<a class="btn btn-clean btn-bold btn-sm" href="#"><i class="flaticon2-add-1 kt-icon-sm"></i> Add new</a>
																		<a class="btn btn-clean btn-bold btn-sm kt-hidden" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
																	</li>
																</ul>
															</div>
														</div>-->
													</div>
													<div class="kt-widget__action">
														<a href="upgrade-membership.php" class="btn btn-label-success btn-sm btn-upper">Assign Grade</a>
                                                        <a href="#" class="btn btn-label-success btn-sm btn-upper">Contact</a>

                                                        <div class="dropdown dropdown-inline">
															<a href="#" class="btn btn-success btn-sm btn-upper dropdown-toggle" data-toggle="dropdown">
																Action
															</a>
															<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

																<!--begin::Nav-->
																<ul class="kt-nav">

																	<li class="kt-nav__separator"></li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon fa fa-check"></i>
																			<span class="kt-nav__link-text">Approve</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon fa fa-times"></i>
																			<span class="kt-nav__link-text">Suspend Account</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
																			<span class="kt-nav__link-text">Settings</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon flaticon2-new-email"></i>
																			<span class="kt-nav__link-text">Support</span>
																			<span class="kt-nav__link-badge">
																				<span class="kt-badge kt-badge--success kt-badge--rounded">5</span>
																			</span>
																		</a>
																	</li>
																	<li class="kt-nav__separator"></li>
																	<li class="kt-nav__foot">
																		<a class="btn btn-label-brand btn-bold btn-sm" href="upgrade-membership.php">Upgrade Membership Garde</a>
																	</li>
																</ul>

																<!--end::Nav-->
															</div>
														</div>
													</div>
												</div>
												<div class="kt-widget__subhead">
													<a href="#"><i class="flaticon2-new-email"></i>david.s@loop.com</a>
													<a href="#"><i class="flaticon2-calendar-3"></i>CYSEC00009 </a>
													<a href="#"><i class="flaticon2-placeholder"></i>Fellow</a>
												</div>
												<div class="kt-widget__info">
													<div class="kt-widget__desc">
														I distinguish three main text objektive could be merely to inform people.
														 A second could be persuade people.You want people to bay objective
													</div>
													<div class="kt-widget__progress">
														<div class="kt-widget__text">
															Progress
														</div>
														<div class="progress" style="height: 5px;width: 100%;">
															<div class="progress-bar kt-bg-success" role="progressbar" style="width: 15%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
														<div class="kt-widget__stats">
															15%
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>

							<!--End:: Portlet-->
							<div class="row">

								<div class="col-xl-12">

									<!--Begin:: Portlet-->
									<div class="kt-portlet kt-portlet--tabs">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-toolbar">
												<ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
													<li class="nav-item">
														<a class="nav-link active" data-toggle="tab" href="#member_personal_info" role="tab">
															<i class="flaticon2-note"></i> Personal Information
														</a>
													</li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#member_personal_address" role="tab">
                                                            <i class="flaticon2-note"></i> Personal Address
                                                        </a>
                                                    </li>

                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#educational_details" role="tab">
                                                            <i class="flaticon2-calendar-3"></i> Educational
                                                        </a>
                                                    </li>
													<li class="nav-item">
														<a class="nav-link" data-toggle="tab" href="#professional_details" role="tab">
															<i class="flaticon2-user-outline-symbol"></i> Professional
														</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" data-toggle="tab" href="#interest_details" role="tab">
															<i class="flaticon2-gear"></i> Interest Area
														</a>
													</li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#working_details" role="tab">
                                                            <i class="flaticon2-gear"></i> Working Experience
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#working_details" role="tab">
                                                            <i class="flaticon2-gear"></i> Attachments
                                                        </a>
                                                    </li>

												</ul>
											</div>
										</div>
										<div class="kt-portlet__body">
											<div class="tab-content kt-margin-t-20">

                                                <!--Begin:: Tab Content2-->
                                                <div class="tab-pane active" id="member_personal_info" role="tabpanel">
                                                    <form class="kt-form kt-form--label-right" action="">
                                                        <div class="kt-form__body">
                                                            <div class="kt-section kt-section--first">
                                                                <div class="kt-section__body">
                                                                    <div class="row">
                                                                        <label class="col-xl-3"></label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <h3 class="kt-section__title kt-section__title-sm"> Personal Info:</h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Photo</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <div class="kt-avatar kt-avatar--outline" id="kt_apps_user_add_avatar">
                                                                                <div class="kt-avatar__holder" style="background-image: url('assets/media/users/100_12.jpg');"></div>
                                                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                                                                    <i class="fa fa-pen"></i>
                                                                                    <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg">
                                                                                </label>
                                                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
																					<i class="fa fa-times"></i>
																				</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Surname Name</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Nick">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Bold">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Other Name</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                       </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Date of Birth</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Gender</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Alt Email Address</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Mobile Contact</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Alt Mobile Contact</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Marital Status</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                                        <div class="kt-form__actions">
                                                            <div class="row">
                                                                <div class="col-xl-3"></div>
                                                                <div class="col-lg-9 col-xl-6">
                                                                    <a href="#" class="btn btn-label-brand btn-bold">Save changes</a>
                                                                    <a href="#" class="btn btn-clean btn-bold">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <!--End:: Tab Content2-->
                                                <!--Begin:: Tab Content2-->
                                                <div class="tab-pane" id="member_personal_address" role="tabpanel">
                                                    <form class="kt-form kt-form--label-right" action="">
                                                        <div class="kt-form__body">
                                                            <div class="kt-section kt-section--first">
                                                                <div class="kt-section__body">
                                                                    <div class="row">
                                                                        <label class="col-xl-3"></label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <h3 class="kt-section__title kt-section__title-sm"> Contact Address:</h3>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Country of Origin:</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Nick">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">State/Province of Origin</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Bold">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Home Address:</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Residential Address</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Residential Country</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Residential State</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Postal Address</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                                        <div class="kt-form__actions">
                                                            <div class="row">
                                                                <div class="col-xl-3"></div>
                                                                <div class="col-lg-9 col-xl-6">
                                                                    <a href="#" class="btn btn-label-brand btn-bold">Save changes</a>
                                                                    <a href="#" class="btn btn-clean btn-bold">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <!--End:: Tab Content2-->

												<!--Begin:: Educational Details-->
												<div class="tab-pane" id="educational_details" role="tabpanel">
													<form class="kt-form kt-form--label-right" action="">
														<div class="kt-form__body">
															<div class="kt-section kt-section--first">
																<div class="kt-section__body">
																	<div class="row">
																		<label class="col-xl-3"></label>
																		<div class="col-lg-9 col-xl-6">
																			<h3 class="kt-section__title kt-section__title-sm">Education:</h3>
																		</div>
																	</div>

																	<div class="form-group row">
																		<label class="col-xl-3 col-lg-3 col-form-label">Institution Name</label>
																		<div class="col-lg-9 col-xl-6">
																			<input class="form-control" type="text" value="Nick">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-xl-3 col-lg-3 col-form-label">Graduation Year</label>
																		<div class="col-lg-9 col-xl-6">
																			<input class="form-control" type="text" value="Bold">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-xl-3 col-lg-3 col-form-label">Educational Level</label>
																		<div class="col-lg-9 col-xl-6">
																			<input class="form-control" type="text" value="Loop Inc.">
																		</div>
																	</div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Qualification</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>

																</div>
															</div>
														</div>
														<div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
														<div class="kt-form__actions">
															<div class="row">
																<div class="col-xl-3"></div>
																<div class="col-lg-9 col-xl-6">
																	<a href="#" class="btn btn-label-brand btn-bold">Save changes</a>
																	<a href="#" class="btn btn-clean btn-bold">Cancel</a>
																</div>
															</div>
														</div>
													</form>
												</div>

												<!--End:: Educational Details-->


                                                <!--Begin:: Professional Details-->
                                                <div class="tab-pane" id="professional_details" role="tabpanel">
                                                    <form class="kt-form kt-form--label-right" action="">
                                                        <div class="kt-form__body">
                                                            <div class="kt-section kt-section--first">
                                                                <div class="kt-section__body">
                                                                    <div class="row">
                                                                        <label class="col-xl-3"></label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <h3 class="kt-section__title kt-section__title-sm">Professional Details:</h3>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Institution Name</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Nick">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Certification Name</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Bold">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Certification Number</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Year Issued</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Year Expiring</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                                        <div class="kt-form__actions">
                                                            <div class="row">
                                                                <div class="col-xl-3"></div>
                                                                <div class="col-lg-9 col-xl-6">
                                                                    <a href="#" class="btn btn-label-brand btn-bold">Save changes</a>
                                                                    <a href="#" class="btn btn-clean btn-bold">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <!--End:: Professional Details-->


                                                <!--Begin:: Interest Details-->
                                                <div class="tab-pane" id="interest_details" role="tabpanel">
                                                    <form class="kt-form kt-form--label-right" action="">
                                                        <div class="kt-form__body">
                                                            <div class="kt-section kt-section--first">
                                                                <div class="kt-section__body">
                                                                    <div class="row">
                                                                        <label class="col-xl-3"></label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <h3 class="kt-section__title kt-section__title-sm">Interest Area:</h3>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Institution Name</label>
                                                                        kjkajksjask<br>
                                                                            sdshdsjdhjs<br>
                                                                            jhjgjjgjg
                                                                    </div>

                                                                    <div class="row">
                                                                        <label class="col-xl-3"></label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <h3 class="kt-section__title kt-section__title-sm">Interest Focus:</h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Institution Name</label>
                                                                        kjkajksjask<br>
                                                                        sdshdsjdhjs<br>
                                                                        jhjgjjgjg
                                                                    </div
                                                                </div></div>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>

                                                <!--End:: Interest Details-->


                                            <!--Begin:: Working Experience-->
                                            <div class="tab-pane" id="working_details" role="tabpanel">
                                                <form class="kt-form kt-form--label-right" action="">
                                                    <div class="kt-form__body">
                                                        <div class="kt-section kt-section--first">
                                                            <div class="kt-section__body">
                                                                <div class="row">
                                                                    <label class="col-xl-3"></label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <h3 class="kt-section__title kt-section__title-sm">Working Experience:</h3>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Organization Name</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="exampleTextarea" class="col-xl-3 col-lg-3 col-form-label">Duties:</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <textarea class="form-control" rows="3"></textarea>                                                                    </div>
                                                                 </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Employee Type</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Employee Type Alternative</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Loop Inc.">
                                                                    </div>
                                                                </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Designation</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>

                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Date Started</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Loop Inc.">
                                                                    </div>
                                                                </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Dated Ended</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <input class="form-control" type="text" value="Loop Inc.">
                                                                        </div>
                                                                    </div>
                                                        </div>
                                                    </div>
                                                    <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                                    <div class="kt-form__actions">
                                                        <div class="row">
                                                            <div class="col-xl-3"></div>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <a href="#" class="btn btn-label-brand btn-bold">Save changes</a>
                                                                <a href="#" class="btn btn-clean btn-bold">Cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <!--End:: Working Experience-->



											</div>
										</div>
									</div>

									<!--End:: Portlet-->
								</div>
							</div>
						</div>

						<!-- end:: Content -->
					</div>
@include("admin.footer")
