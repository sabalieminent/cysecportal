<?php include("header.blade.php") ?>
<!-- begin:: Subheader -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-container  kt-container--fluid ">
								<div class="kt-subheader__main">
									<h3 class="kt-subheader__title">
										Profile 3 </h3>
									<span class="kt-subheader__separator kt-hidden"></span>
									<div class="kt-subheader__breadcrumbs">
										<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="" class="kt-subheader__breadcrumbs-link">
											Applications </a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="" class="kt-subheader__breadcrumbs-link">
											Users </a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="" class="kt-subheader__breadcrumbs-link">
											Profile 3 </a>

										<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
									</div>
								</div>

							</div>
						</div>

						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

							<!--Begin::Section-->
							<div class="row">
								<div class="col-xl-12">

									<!--begin:: Widgets/Applications/User/Profile3-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__body">
											<div class="kt-widget kt-widget--user-profile-3">
												<div class="kt-widget__top">
													<div class="kt-widget__media kt-hidden-">
														<img src="assets/media/users/100_13.jpg" alt="image">
													</div>
													<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
														JM
													</div>
													<div class="kt-widget__content">
														<div class="kt-widget__head">
															<a href="#" class="kt-widget__username">
																Jason Muller
																<i class="flaticon2-correct"></i>
															</a>
															<div class="kt-widget__action">
                                                                <a href="user-edit.php"> <button type="button" class="btn btn-label-success btn-sm btn-upper">Edit Information</button>&nbsp;</a>
                                                                <a href="change-password.php"> <button type="button" class="btn btn-brand btn-sm btn-upper">Change Password</button></a>
															</div>
														</div>
														<div class="kt-widget__subhead">
                                                            <a href="#"><i class="flaticon2-new-email"></i><strong>Email:</strong> jason@siastudio.com</a>
															<a href="#"><i class="flaticon2-calendar-3"></i><strong> Status Level:</strong> Admin </a>
															<a href="#"><i class="flaticon2-placeholder"></i><strong>Mobile Contact:</strong> 09909093</a>
														</div>


													</div>
												</div>

											</div>
										</div>
                                    </div></div></div></div></div>


							<!--End::Section-->



					<?php include('footer.blade.php') ?>
		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/dashboard.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
