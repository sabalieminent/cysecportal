<?php include("header.blade.php") ?>

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">

								<!--Begin::App-->
								<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

									<!--Begin:: App Aside Mobile Toggle-->
									<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
										<i class="la la-close"></i>
									</button>

									<!--End:: App Aside Mobile Toggle-->

									<!--Begin:: App Aside-->
									<div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

										<?php include ("side-menu.php")?>
									</div>

									<!--End:: App Aside-->

									<!--Begin:: App Content-->
                                    <div class="kt-portlet" style="margin-left: 10px">
                                        <div class="kt-portlet__head kt-portlet__head--lg">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    <strong>Working Experience</strong>
                                                </h3>


                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                            <div class="kt-portlet__head-wrapper">
                                                <div class="kt-portlet__head-actions">

                                                    <a href="working.php" class="btn btn-brand btn-elevate btn-icon-sm">
                                                        <i class="la la-plus"></i>
                                                        Add New Record
                                                    </a>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">

                                            <!--begin::Section-->
                                            <div class="kt-section">
                                                <div class="kt-section__content">
                                                    <table class="table table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th style="font-weight: bold">#</th>
                                                            <th style="font-weight: bold">Organization </th>
                                                            <th style="font-weight: bold">Duties</th>
                                                            <th style="font-weight: bold">Employer Type</th>
                                                            <th style="font-weight: bold">Designation</th>
                                                            <th style="font-weight: bold">Start Date</th>
                                                            <th style="font-weight: bold">End Date</th>
                                                            <th style="font-weight: bold">Edit</th>
                                                            <th style="font-weight: bold">Delete</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <th scope="row">1</th>
                                                            <td>University of Nigeria Nsukka</td>
                                                            <td>2020</td>
                                                            <td>B.Sc</td>
                                                            <td>SDff</td>
                                                            <td>2020</td>
                                                            <td>B.Sc</td>
                                                            <td>
                                                                <button class="btn btn-success fa fa-edit" data-toggle="modal" data-target="#kt_modal_4"> Edit </button>
                                                            </td>
                                                            <td>
                                                                <button id="kt_sweetalert_demo_9" type="button" class="btn btn-danger la la-trash " id="#"> Delete </button>
                                                            </td>
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <!--end::Section-->
                                        </div>

                                        <!--end::Form-->
                                    </div>
									<!--End:: App Content-->
								</div></div>

								<!--End::App-->
							</div>
</div>
<div class="modal fade" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Your Working Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Organization Name:</label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="form-group">
                        <label  class="form-control-label">Duties:</label>
                        <textarea class="form-control" type="text" rows="2"> </textarea>
                    </div>

                                      <div class="row">
                        <div class="col-xl-6">
                            <div class="form-group">
                                <label>Employer Type</label>
                                <select name="" class="form-control" aria-describedby="country-error" aria-invalid="false">
                                    <option value="">Select</option>
                                    <option value="AF">Afghanistan</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group">
                                <label>Designation</label>
                                <input type="text" class="form-control" name="state" placeholder="City" value="London">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="date" class="form-control" name="">
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group">
                                <label>End Date</label>
                                <input type="text" class="form-control" name="">
                                <span><code>Input Till Date You're Still with the present job </code></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-success" id="kt_sweetalert_demo_3_3">Update</button>
            </div>
        </div>
    </div>
</div>
							<!-- end:: Content -->
						</div>
					</div>

					<?php include ("footer.php") ?>

		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/dashboard.js" type="text/javascript"></script>
		<script src="assets/js/pages/custom/user/profile.js" type="text/javascript"></script>
<script src="assets/js/pages/components/extended/sweetalert2.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
