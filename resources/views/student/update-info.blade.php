@include("student.header")
<!--begin::Page Custom Styles(used by this page) -->
<link href="student_assets/css/pages/wizard/wizard-4.css" rel="stylesheet" type="text/css" />
                            <!-- begin:: Content -->
                            <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                                <div class="col-md-10 offset-1">
                                    <!--begin: Form Wizard Form-->
                                    <form class="kt-form"  action="{{ route('update_info.store', $user_data ?? ''->id) }}" method="POST">

                                    @csrf

                                    <!--begin: Form Wizard Step 1-->


                                        <div class="kt-heading kt-heading--md">Enter your Bio Data</div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v4__form">
                                                <div class="form-group ">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                                            <div class="kt-avatar__holder" style="background-image: url(assets/media/users/300_10.jpg)"></div>
                                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Change avatar">
                                                                <i class="fa fa-pen"></i>
                                                                <input type="file" name="kt_user_add_user_avatar">
                                                            </label>
                                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Cancel avatar">
                                                                                    <i class="fa fa-times"></i>
                                                                                </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Last Name</label>
                                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ auth()->user()->last_name }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ auth()->user()->first_name }}">
                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="form-group">
                                                            <label>Phone</label>
                                                            <input type="tel" class="form-control" name="phone" placeholder="phone" >

                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="email" class="form-control" name="email" placeholder="Email" value="{{ auth()->user()->email }}">

                                                        </div>
                                                    </div>



                                                </div>
                                                <div class="form-group">
                                                    <label>Address Line</label>
                                                    <input type="text" class="form-control" name="address" placeholder="Address Line">
                                                </div>

                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="form-group">
                                                            <label>Postcode</label>
                                                            <input type="text" class="form-control" name="post_code" placeholder="Postcode">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="form-group">
                                                            <label>City</label>
                                                            <input type="text" class="form-control" name="state" placeholder="City" >
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="form-group">
                                                            <label>State</label>
                                                            <input type="text" class="form-control" name="state" placeholder="State" >
                                                            <span class="form-text text-muted">Please enter your Postcode.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="form-group">
                                                            <label>Country:</label>
                                                            <select name="country" class="form-control" >
                                                                <option disabled selected>Choose Country</option>
                                                                @foreach($country as $countries)
                                                                    <option value="{{ $countries->id }}">{{ $countries->country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-xl-8">
                                                        <div class="form-group">
                                                            <label>Bio</label>
                                                            <textarea type="number" rows="10" cols="100" class="form-control" name="bio" ></textarea>
                                                        </div>
                                                    </div>

                                                </div>

                                                <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" >
                                                    Submit
                                                </button>
                                            </div>
                                        </div>

                                        <!--end: Form Wizard Step 1-->
                                    </form>
                                    <!--end: Form Wizard Form-->

                                </div>


                            </div>
<br><br>

                            <!-- end:: Content -->

@include("student.footer")


		<!--begin::Page Scripts(used by this page) -->
		<script src="student_assets/js/pages/custom/wizard/wizard-4.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
