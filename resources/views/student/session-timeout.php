<?php include("header.blade.php") ?>

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="alert alert-light alert-elevate" role="alert">
									<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
									<div class="alert-text">
										Session timeout and keep-alive control with a nice Bootstrap warning dialog.
										To learn more please check out
										<a href="https://github.com/orangehill/bootstrap-session-timeout" class="kt-link kt-link--brand kt-font-bold" target="_blank">
											the plugin's official homepage
										</a>
									</div>
								</div>

								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon kt-hidden">
												<i class="la la-gear"></i>
											</span>
											<h3 class="kt-portlet__head-title">
												Session Timeout Demo
											</h3>
										</div>
									</div>
									<div class="kt-portlet__body">
										After a set amount of time(10 seconds set for demo), a dialog is shown to the user with the option to either log out now, or stay connected.
										If log out now is selected, the page is redirected to a logout URL. If stay connected is selected, a keep-alive URL is requested through AJAX.
										If no options is selected after another set amount of time, the page is automatically redirected to a timeout URL.
									</div>
								</div>

								<!--end::Portlet-->
							</div>

							<!-- end:: Content -->
						</div>
					</div>

					<?php include ("footer.php") ?>
		<script src="assets/js/pages/components/utils/session-timeout.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
