<?php include("header.blade.php") ?>

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="alert alert-light alert-elevate" role="alert">
									<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
									<div class="alert-text">
										jQuery Idle Timer fires a custom event when the user is "idle". To learn more please check out
										<a href="https://github.com/thorst/jquery-idletimer" class="kt-link kt-link--brand kt-font-bold" target="_blank">
											the plugin's official homepage
										</a>
									</div>
								</div>

								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon kt-hidden">
												<i class="la la-gear"></i>
											</span>
											<h3 class="kt-portlet__head-title">
												jQuery Idle Timer
											</h3>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											The idle timer is built on jQuery and provides two events: <code>idle.idleTimer</code> and <code>active.idleTimer</code>,
											which fire when the user's idle state has changed.
											When you move your mouse over the page or start typing, you're considered <code>active</code>.
											On this page we have two idle timers. One for the entire document. Another for the text area on the right (or bottom if your on mobile).
										</div>
									</div>
								</div>

								<!--end::Portlet-->
								<div class="row">
									<div class="col-lg-6">

										<!--begin::Portlet-->
										<div class="kt-portlet">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">
													<span class="kt-portlet__head-icon kt-hidden">
														<i class="la la-gear"></i>
													</span>
													<h3 class="kt-portlet__head-title">
														Document <small><span id="docTimeout"></span> second timeout</small>
													</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="btn-group kt-margin-b-10">
													<a href="javascript:;" id="btPause" class="btn btn-secondary">Pause</a>
													<a href="javascript:;" id="btResume" class="btn btn-secondary">Resume</a>
													<a href="javascript:;" id="btElapsed" class="btn btn-secondary">Elapsed</a>
													<a href="javascript:;" id="btInit" class="btn btn-secondary">Init</a>
													<a href="javascript:;" id="btDestroy" class="btn btn-secondary">Destroy</a>
												</div>
												<textarea rows="10" cols="30" id="docStatus" class="form-control"></textarea><br />
											</div>
										</div>

										<!--end::Portlet-->
									</div>
									<div class="col-lg-6">

										<!--begin::Portlet-->
										<div class="kt-portlet">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">
													<span class="kt-portlet__head-icon kt-hidden">
														<i class="la la-gear"></i>
													</span>
													<h3 class="kt-portlet__head-title">
														Element <small><span id="elTimeout"></span> second timeout</small>
													</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="btn-group kt-margin-b-10">
													<a href="javascript:;" id="btReset" class="btn btn-secondary">Reset</a>
													<a href="javascript:;" id="btLastActive" class="btn btn-secondary">Last Active</a>
													<a href="javascript:;" id="btRemaining" class="btn btn-secondary">Remaining</a>
													<a href="javascript:;" id="btState" class="btn btn-secondary">State</a>
												</div>
												<textarea rows="10" cols="30" id="elStatus" class="form-control"></textarea><br />
											</div>
										</div>

										<!--end::Portlet-->
									</div>
								</div>

								<!--end::Portlet-->
							</div>

							<!-- end:: Content -->
						</div>
					</div>

					<?php include "footer.php"?>
		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/components/utils/idle-timer.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
