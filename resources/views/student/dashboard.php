<?php
include("header.blade.php") ?>

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">

								<!--begin:: Portlet-->
								<div class="kt-portlet ">
									<div class="kt-portlet__body">
										<div class="kt-widget kt-widget--user-profile-3">
											<div class="kt-widget__top">
												<div class="kt-widget__media kt-hidden-">
													<img src="assets/media/project-logos/3.png" alt="image">
												</div>
												<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
													JM
												</div>
												<div class="kt-widget__content">
													<div class="kt-widget__head">
														<a href="#" class="kt-widget__username kt-hidden">
															Jason Muller
															<i class="flaticon2-correct"></i>
														</a>
														<a href="#" class="kt-widget__title">Nexa - Next generation SAAS</a>
														<div class="kt-widget__action">
															<button type="button" class="btn btn-sm btn-upper" style="background: #edeff6">edit</button>&nbsp;
															<button type="button" class="btn btn-success btn-sm btn-upper">add user</button>&nbsp;
															<button type="button" class="btn btn-brand btn-sm btn-upper">new task</button>
														</div>
													</div>
													<div class="kt-widget__subhead kt-hidden">
														<a href="#"><i class="flaticon2-new-email"></i>jason@siastudio.com</a>
														<a href="#"><i class="flaticon2-calendar-3"></i>PR Manager </a>
														<a href="#"><i class="flaticon2-placeholder"></i>Melbourne</a>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
															I distinguish three main text objektive.First, your could
															<br> be merely to inform people a second could be persuade people.
															<br> You want people to bay your products
														</div>
														<div class="kt-widget__progress kt-hidden">
															<div class="kt-widget__text">
																Progress
															</div>
															<div class="progress" style="height: 5px;width: 100%;">
																<div class="progress-bar kt-bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
															</div>
															<div class="kt-widget__stats">
																78%
															</div>
														</div>
														<div class="kt-widget__stats d-flex align-items-center flex-fill">
															<div class="kt-widget__item">
																<span class="kt-widget__date">
																	Start Date
																</span>
																<div class="kt-widget__label">
																	<span class="btn btn-label-brand btn-sm btn-bold btn-upper">07 may, 18</span>
																</div>
															</div>
															<div class="kt-widget__item">
																<span class="kt-widget__date">
																	Due Date
																</span>
																<div class="kt-widget__label">
																	<span class="btn btn-label-danger btn-sm btn-bold btn-upper">07 0ct, 18</span>
																</div>
															</div>
															<div class="kt-widget__item flex-fill">
																<span class="kt-widget__subtitel">Progress</span>
																<div class="kt-widget__progress d-flex  align-items-center">
																	<div class="progress" style="height: 5px;width: 100%;">
																		<div class="progress-bar kt-bg-success" role="progressbar" style="width: 78%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
																	</div>
																	<span class="kt-widget__stat">
																		78%
																	</span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="kt-widget__bottom">
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-piggy-bank"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Earnings</span>
														<span class="kt-widget__value"><span>$</span>249,500</span>
													</div>
												</div>
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-confetti"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Expances</span>
														<span class="kt-widget__value"><span>$</span>164,700</span>
													</div>
												</div>
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-pie-chart"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Net</span>
														<span class="kt-widget__value"><span>$</span>84,060</span>
													</div>
												</div>
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-file-2"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">73 Tasks</span>
														<a href="#" class="kt-widget__value kt-font-brand">View</a>
													</div>
												</div>
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-chat-1"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">648 Comments</span>
														<a href="#" class="kt-widget__value kt-font-brand">View</a>
													</div>
												</div>
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-network"></i>
													</div>
													<div class="kt-widget__details">
														<div class="kt-section__content kt-section__content--solid">
															<div class="kt-media-group">
																<a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="John Myer">
																	<img src="assets/media/users/100_7.jpg" alt="image">
																</a>
																<a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Alison Brandy">
																	<img src="assets/media/users/100_3.jpg" alt="image">
																</a>
																<a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Selina Cranson">
																	<img src="assets/media/users/100_2.jpg" alt="image">
																</a>
																<a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Luke Walls">
																	<img src="assets/media/users/100_13.jpg" alt="image">
																</a>
																<a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Micheal York">
																	<img src="assets/media/users/100_4.jpg" alt="image">
																</a>
																<a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Micheal York">
																	<span>+3</span>
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<!--end:: Portlet-->
								<div class="row">
									<div class="col-xl-6">

										<!--begin:: Widgets/Order Statistics-->
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title">
														Order Statistics
													</h3>
												</div>
												<div class="kt-portlet__head-toolbar">
													<a href="#" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
														Export
													</a>
													<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">

														<!--begin::Nav-->
														<ul class="kt-nav">
															<li class="kt-nav__head">
																Export Options
																<span data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand kt-svg-icon--md1">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
																			<rect fill="#000000" x="11" y="10" width="2" height="7" rx="1" />
																			<rect fill="#000000" x="11" y="7" width="2" height="2" rx="1" />
																		</g>
																	</svg> </span>
															</li>
															<li class="kt-nav__separator"></li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-drop"></i>
																	<span class="kt-nav__link-text">Activity</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
																	<span class="kt-nav__link-text">FAQ</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
																	<span class="kt-nav__link-text">Settings</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-new-email"></i>
																	<span class="kt-nav__link-text">Support</span>
																	<span class="kt-nav__link-badge">
																		<span class="kt-badge kt-badge--success kt-badge--rounded">5</span>
																	</span>
																</a>
															</li>
															<li class="kt-nav__separator"></li>
															<li class="kt-nav__foot">
																<a class="btn btn-label-danger btn-bold btn-sm" href="#">Upgrade plan</a>
																<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
															</li>
														</ul>

														<!--end::Nav-->
													</div>
												</div>
											</div>
											<div class="kt-portlet__body kt-portlet__body--fluid">
												<div class="kt-widget12">
													<div class="kt-widget12__content">
														<div class="kt-widget12__item">
															<div class="kt-widget12__info">
																<span class="kt-widget12__desc">Annual Taxes EMS</span>
																<span class="kt-widget12__value">$400,000</span>
															</div>
															<div class="kt-widget12__info">
																<span class="kt-widget12__desc">Finance Review Date</span>
																<span class="kt-widget12__value">July 24,2019</span>
															</div>
														</div>
														<div class="kt-widget12__item">
															<div class="kt-widget12__info">
																<span class="kt-widget12__desc">Avarage Revenue</span>
																<span class="kt-widget12__value">$60M</span>
															</div>
															<div class="kt-widget12__info">
																<span class="kt-widget12__desc">Revenue Margin</span>
																<div class="kt-widget12__progress">
																	<div class="progress kt-progress--sm">
																		<div class="progress-bar kt-bg-brand" role="progressbar" style="width: 40%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
																	</div>
																	<span class="kt-widget12__stat">
																		40%
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="kt-widget12__chart" style="height:250px;">
														<canvas id="kt_chart_order_statistics"></canvas>
													</div>
												</div>
											</div>
										</div>

										<!--end:: Widgets/Order Statistics-->
									</div>
									<div class="col-xl-6">

										<!--begin:: Widgets/Tasks -->
										<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title">
														Tasks
													</h3>
												</div>
												<div class="kt-portlet__head-toolbar">
													<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
														<li class="nav-item">
															<a class="nav-link active" data-toggle="tab" href="#kt_widget2_tab1_content" role="tab">
																Today
															</a>
														</li>
														<li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#kt_widget2_tab2_content" role="tab">
																Week
															</a>
														</li>
														<li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#kt_widget2_tab3_content" role="tab">
																Month
															</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="tab-content">
													<div class="tab-pane active" id="kt_widget2_tab1_content">
														<div class="kt-widget2">
															<div class="kt-widget2__item kt-widget2__item--primary">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Metronic Great Again.Lorem Ipsum Amet
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Bob
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--warning">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Prepare Docs For Metting On Monday
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Sean
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--brand">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Widgets Development. Estudiat Communy Elit
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Aziko
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--success">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Metronic Development. Lorem Ipsum
																	</a>
																	<a class="kt-widget2__username">
																		By James
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--danger">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Completa Financial Report For Emirates Airlines
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Bob
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--info">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Completa Financial Report For Emirates Airlines
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Sean
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="tab-pane" id="kt_widget2_tab2_content">
														<div class="kt-widget2">
															<div class="kt-widget2__item kt-widget2__item--success">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Metronic Development.Lorem Ipsum
																	</a>
																	<a class="kt-widget2__username">
																		By James
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--warning">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Prepare Docs For Metting On Monday
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Sean
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--danger">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Completa Financial Report For Emirates Airlines
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Bob
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--primary">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Metronic Great Again.Lorem Ipsum Amet
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Bob
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--info">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Completa Financial Report For Emirates Airlines
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Sean
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--brand">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Widgets Development.Estudiat Communy Elit
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Aziko
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="tab-pane" id="kt_widget2_tab3_content">
														<div class="kt-widget2">
															<div class="kt-widget2__item kt-widget2__item--warning">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Metronic Development. Lorem Ipsum
																	</a>
																	<a class="kt-widget2__username">
																		By James
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--danger">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Completa Financial Report For Emirates Airlines
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Bob
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--brand">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Widgets Development.Estudiat Communy Elit
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Aziko
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--info">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Completa Financial Report For Emirates Airlines
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Sean
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--success">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Completa Financial Report For Emirates Airlines
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Bob
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="kt-widget2__item kt-widget2__item--primary">
																<div class="kt-widget2__checkbox">
																	<label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
																		<input type="checkbox">
																		<span></span>
																	</label>
																</div>
																<div class="kt-widget2__info">
																	<a href="#" class="kt-widget2__title">
																		Make Metronic Great Again.Lorem Ipsum Amet
																	</a>
																	<a href="#" class="kt-widget2__username">
																		By Bob
																	</a>
																</div>
																<div class="kt-widget2__actions">
																	<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
																		<i class="flaticon-more-1"></i>
																	</a>
																	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
																		<ul class="kt-nav">
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-line-chart"></i>
																					<span class="kt-nav__link-text">Reports</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-send"></i>
																					<span class="kt-nav__link-text">Messages</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
																					<span class="kt-nav__link-text">Charts</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-avatar"></i>
																					<span class="kt-nav__link-text">Members</span>
																				</a>
																			</li>
																			<li class="kt-nav__item">
																				<a href="#" class="kt-nav__link">
																					<i class="kt-nav__link-icon flaticon2-settings"></i>
																					<span class="kt-nav__link-text">Settings</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<!--end:: Widgets/Tasks -->
									</div>
								</div>
								<div class="row">
									<div class="col-xl-6">

										<!--begin:: Widgets/Last Updates-->
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title">
														Latest Updates
													</h3>
												</div>
												<div class="kt-portlet__head-toolbar">
													<a href="#" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
														Today
													</a>
													<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

														<!--begin::Nav-->
														<ul class="kt-nav">
															<li class="kt-nav__head">
																Export Options
																<span data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand kt-svg-icon--md1">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
																			<rect fill="#000000" x="11" y="10" width="2" height="7" rx="1" />
																			<rect fill="#000000" x="11" y="7" width="2" height="2" rx="1" />
																		</g>
																	</svg> </span>
															</li>
															<li class="kt-nav__separator"></li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-drop"></i>
																	<span class="kt-nav__link-text">Activity</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
																	<span class="kt-nav__link-text">FAQ</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
																	<span class="kt-nav__link-text">Settings</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-new-email"></i>
																	<span class="kt-nav__link-text">Support</span>
																	<span class="kt-nav__link-badge">
																		<span class="kt-badge kt-badge--success kt-badge--rounded">5</span>
																	</span>
																</a>
															</li>
															<li class="kt-nav__separator"></li>
															<li class="kt-nav__foot">
																<a class="btn btn-label-danger btn-bold btn-sm" href="#">Upgrade plan</a>
																<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
															</li>
														</ul>

														<!--end::Nav-->
													</div>
												</div>
											</div>
											<div class="kt-portlet__body">

												<!--begin::widget 12-->
												<div class="kt-widget4">
													<div class="kt-widget4__item">
														<span class="kt-widget4__icon">
															<i class="flaticon-pie-chart-1 kt-font-info"></i>
														</span>
														<a href="#" class="kt-widget4__title kt-widget4__title--light">
															Metronic v6 has been arrived!
														</a>
														<span class="kt-widget4__number kt-font-info">+500</span>
													</div>
													<div class="kt-widget4__item">
														<span class="kt-widget4__icon">
															<i class="flaticon-safe-shield-protection  kt-font-success"></i>
														</span>
														<a href="#" class="kt-widget4__title kt-widget4__title--light">
															Metronic community meet-up 2019 in Rome.
														</a>
														<span class="kt-widget4__number kt-font-success">+1260</span>
													</div>
													<div class="kt-widget4__item">
														<span class="kt-widget4__icon">
															<i class="flaticon2-line-chart kt-font-danger"></i>
														</span>
														<a href="#" class="kt-widget4__title kt-widget4__title--light">
															Metronic Angular 8 version will be landing soon...
														</a>
														<span class="kt-widget4__number kt-font-danger">+1080</span>
													</div>
													<div class="kt-widget4__item">
														<span class="kt-widget4__icon">
															<i class="flaticon2-pie-chart-1 kt-font-primary"></i>
														</span>
														<a href="#" class="kt-widget4__title kt-widget4__title--light">
															ale! Purchase Metronic at 70% off for limited time
														</a>
														<span class="kt-widget4__number kt-font-primary">70% Off!</span>
													</div>
													<div class="kt-widget4__item">
														<span class="kt-widget4__icon">
															<i class="flaticon2-rocket kt-font-brand"></i>
														</span>
														<a href="#" class="kt-widget4__title kt-widget4__title--light">
															Metronic VueJS version is in progress. Stay tuned!
														</a>
														<span class="kt-widget4__number kt-font-brand">+134</span>
													</div>
													<div class="kt-widget4__item">
														<span class="kt-widget4__icon">
															<i class="flaticon2-notification kt-font-warning"></i>
														</span>
														<a href="#" class="kt-widget4__title kt-widget4__title--light">
															Black Friday! Purchase Metronic at ever lowest 90% off for limited time
														</a>
														<span class="kt-widget4__number kt-font-warning">70% Off!</span>
													</div>
													<div class="kt-widget4__item">
														<span class="kt-widget4__icon">
															<i class="flaticon2-file kt-font-success"></i>
														</span>
														<a href="#" class="kt-widget4__title kt-widget4__title--light">
															Metronic React version is in progress.
														</a>
														<span class="kt-widget4__number kt-font-success">+13%</span>
													</div>
												</div>

												<!--end::Widget 12-->
											</div>
										</div>

										<!--end:: Widgets/Last Updates-->
									</div>
									<div class="col-xl-6">

										<!--begin:: Widgets/Notifications-->
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title">
														Notifications
													</h3>
												</div>
												<div class="kt-portlet__head-toolbar">
													<ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
														<li class="nav-item">
															<a class="nav-link active" data-toggle="tab" href="#kt_widget6_tab1_content" role="tab">
																Latest
															</a>
														</li>
														<li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#kt_widget6_tab2_content" role="tab">
																Week
															</a>
														</li>
														<li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#kt_widget6_tab3_content" role="tab">
																Month
															</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="tab-content">
													<div class="tab-pane active" id="kt_widget6_tab1_content" aria-expanded="true">
														<div class="kt-notification">
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000" />
																			<rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New order has been received.
																	</div>
																	<div class="kt-notification__item-time">
																		2 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.98630124,11 4.48466491,11.0516454 4,11.1500272 L4,7 C4,5.8954305 4.8954305,5 6,5 L20,5 C21.1045695,5 22,5.8954305 22,7 L22,16 C22,17.1045695 21.1045695,18 20,18 L12.9835977,18 Z M19.1444251,6.83964668 L13,10.1481833 L6.85557487,6.83964668 C6.4908718,6.6432681 6.03602525,6.77972206 5.83964668,7.14442513 C5.6432681,7.5091282 5.77972206,7.96397475 6.14442513,8.16035332 L12.6444251,11.6603533 C12.8664074,11.7798822 13.1335926,11.7798822 13.3555749,11.6603533 L19.8555749,8.16035332 C20.2202779,7.96397475 20.3567319,7.5091282 20.1603533,7.14442513 C19.9639747,6.77972206 19.5091282,6.6432681 19.1444251,6.83964668 Z" fill="#000000" />
																			<path d="M8.4472136,18.1055728 C8.94119209,18.3525621 9.14141644,18.9532351 8.89442719,19.4472136 C8.64743794,19.9411921 8.0467649,20.1414164 7.5527864,19.8944272 L5,18.618034 L5,14.5 C5,13.9477153 5.44771525,13.5 6,13.5 C6.55228475,13.5 7,13.9477153 7,14.5 L7,17.381966 L8.4472136,18.1055728 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New member is registered and pending for approval.
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M12,10.9996338 C12.8356605,10.3719448 13.8743941,10 15,10 C17.7614237,10 20,12.2385763 20,15 C20,17.7614237 17.7614237,20 15,20 C13.8743941,20 12.8356605,19.6280552 12,19.0003662 C11.1643395,19.6280552 10.1256059,20 9,20 C6.23857625,20 4,17.7614237 4,15 C4,12.2385763 6.23857625,10 9,10 C10.1256059,10 11.1643395,10.3719448 12,10.9996338 Z M13.3336047,12.504354 C13.757474,13.2388026 14,14.0910788 14,15 C14,15.9088933 13.7574889,16.761145 13.3336438,17.4955783 C13.8188886,17.8206693 14.3938466,18 15,18 C16.6568542,18 18,16.6568542 18,15 C18,13.3431458 16.6568542,12 15,12 C14.3930587,12 13.8175971,12.18044 13.3336047,12.504354 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																			<circle fill="#000000" cx="12" cy="9" r="5" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Membership application has been added.
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<polygon points="0 0 24 0 24 24 0 24" />
																			<path d="M18.5,8 C17.1192881,8 16,6.88071187 16,5.5 C16,4.11928813 17.1192881,3 18.5,3 C19.8807119,3 21,4.11928813 21,5.5 C21,6.88071187 19.8807119,8 18.5,8 Z M18.5,21 C17.1192881,21 16,19.8807119 16,18.5 C16,17.1192881 17.1192881,16 18.5,16 C19.8807119,16 21,17.1192881 21,18.5 C21,19.8807119 19.8807119,21 18.5,21 Z M5.5,21 C4.11928813,21 3,19.8807119 3,18.5 C3,17.1192881 4.11928813,16 5.5,16 C6.88071187,16 8,17.1192881 8,18.5 C8,19.8807119 6.88071187,21 5.5,21 Z" fill="#000000" opacity="0.3" />
																			<path d="M5.5,8 C4.11928813,8 3,6.88071187 3,5.5 C3,4.11928813 4.11928813,3 5.5,3 C6.88071187,3 8,4.11928813 8,5.5 C8,6.88071187 6.88071187,8 5.5,8 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 C14,5.55228475 13.5522847,6 13,6 L11,6 C10.4477153,6 10,5.55228475 10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,18 L13,18 C13.5522847,18 14,18.4477153 14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 C10,18.4477153 10.4477153,18 11,18 Z M5,10 C5.55228475,10 6,10.4477153 6,11 L6,13 C6,13.5522847 5.55228475,14 5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 C18.4477153,14 18,13.5522847 18,13 L18,11 C18,10.4477153 18.4477153,10 19,10 Z" fill="#000000" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New report file has been uploaded.
																	</div>
																	<div class="kt-notification__item-time">
																		5 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M16.3740377,19.9389434 L22.2226499,11.1660251 C22.4524142,10.8213786 22.3592838,10.3557266 22.0146373,10.1259623 C21.8914367,10.0438285 21.7466809,10 21.5986122,10 L17,10 L17,4.47708173 C17,4.06286817 16.6642136,3.72708173 16.25,3.72708173 C15.9992351,3.72708173 15.7650616,3.85240758 15.6259623,4.06105658 L9.7773501,12.8339749 C9.54758575,13.1786214 9.64071616,13.6442734 9.98536267,13.8740377 C10.1085633,13.9561715 10.2533191,14 10.4013878,14 L15,14 L15,19.5229183 C15,19.9371318 15.3357864,20.2729183 15.75,20.2729183 C16.0007649,20.2729183 16.2349384,20.1475924 16.3740377,19.9389434 Z" fill="#000000" />
																			<path d="M4.5,5 L9.5,5 C10.3284271,5 11,5.67157288 11,6.5 C11,7.32842712 10.3284271,8 9.5,8 L4.5,8 C3.67157288,8 3,7.32842712 3,6.5 C3,5.67157288 3.67157288,5 4.5,5 Z M4.5,17 L9.5,17 C10.3284271,17 11,17.6715729 11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L4.5,20 C3.67157288,20 3,19.3284271 3,18.5 C3,17.6715729 3.67157288,17 4.5,17 Z M2.5,11 L6.5,11 C7.32842712,11 8,11.6715729 8,12.5 C8,13.3284271 7.32842712,14 6.5,14 L2.5,14 C1.67157288,14 1,13.3284271 1,12.5 C1,11.6715729 1.67157288,11 2.5,11 Z" fill="#000000" opacity="0.3" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New user feedback received and pending for review.
																	</div>
																	<div class="kt-notification__item-time">
																		8 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000" />
																			<circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Database sever #2 has been fully restarted.
																	</div>
																	<div class="kt-notification__item-time">
																		23 hrs ago
																	</div>
																</div>
															</a>
														</div>
													</div>
													<div class="tab-pane" id="kt_widget6_tab2_content" aria-expanded="false">
														<div class="kt-notification">
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M9.35321926,16.3736278 L16.3544311,10.3706602 C16.5640654,10.1909158 16.5882961,9.87526197 16.4085517,9.66562759 C16.3922584,9.64662485 16.3745611,9.62887247 16.3556091,9.6125202 L9.35439731,3.57169798 C9.14532254,3.39130299 8.82959492,3.41455255 8.64919993,3.62362732 C8.5708616,3.71442013 8.52776329,3.83034375 8.52776329,3.95026134 L8.52776329,15.9940512 C8.52776329,16.2701936 8.75162092,16.4940512 9.02776329,16.4940512 C9.14714624,16.4940512 9.2625893,16.4513356 9.35321926,16.3736278 Z" fill="#000000" transform="translate(12.398118, 9.870355) rotate(-450.000000) translate(-12.398118, -9.870355) " />
																			<rect fill="#000000" opacity="0.3" transform="translate(12.500000, 17.500000) scale(-1, 1) rotate(-270.000000) translate(-12.500000, -17.500000) " x="11" y="11" width="3" height="13" rx="0.5" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New company application has been approved.
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<rect fill="#000000" opacity="0.3" x="12" y="4" width="3" height="13" rx="1.5" />
																			<rect fill="#000000" opacity="0.3" x="7" y="9" width="3" height="8" rx="1.5" />
																			<path d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z" fill="#000000" fill-rule="nonzero" />
																			<rect fill="#000000" opacity="0.3" x="17" y="11" width="3" height="6" rx="1.5" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New report has been received.
																	</div>
																	<div class="kt-notification__item-time">
																		23 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000" />
																			<rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New file has been uploaded and pending for review.
																	</div>
																	<div class="kt-notification__item-time">
																		5 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M12,10.9996338 C12.8356605,10.3719448 13.8743941,10 15,10 C17.7614237,10 20,12.2385763 20,15 C20,17.7614237 17.7614237,20 15,20 C13.8743941,20 12.8356605,19.6280552 12,19.0003662 C11.1643395,19.6280552 10.1256059,20 9,20 C6.23857625,20 4,17.7614237 4,15 C4,12.2385763 6.23857625,10 9,10 C10.1256059,10 11.1643395,10.3719448 12,10.9996338 Z M13.3336047,12.504354 C13.757474,13.2388026 14,14.0910788 14,15 C14,15.9088933 13.7574889,16.761145 13.3336438,17.4955783 C13.8188886,17.8206693 14.3938466,18 15,18 C16.6568542,18 18,16.6568542 18,15 C18,13.3431458 16.6568542,12 15,12 C14.3930587,12 13.8175971,12.18044 13.3336047,12.504354 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																			<circle fill="#000000" cx="12" cy="9" r="5" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Membership application has been added.
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--info">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<polygon points="0 0 24 0 24 24 0 24" />
																			<path d="M12,4.25932872 C12.1488635,4.25921584 12.3000368,4.29247316 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 L12,4.25932872 Z" fill="#000000" opacity="0.3" />
																			<path d="M12,4.25932872 L12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.277344,4.464261 11.6315987,4.25960807 12,4.25932872 Z" fill="#000000" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer is registered.
																	</div>
																	<div class="kt-notification__item-time">
																		3 days ago
																	</div>
																</div>
															</a>
														</div>
													</div>
													<div class="tab-pane" id="kt_widget6_tab3_content" aria-expanded="false">
														<div class="kt-notification">
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3" />
																			<path d="M14.5,11 C15.0522847,11 15.5,11.4477153 15.5,12 L15.5,15 C15.5,15.5522847 15.0522847,16 14.5,16 L9.5,16 C8.94771525,16 8.5,15.5522847 8.5,15 L8.5,12 C8.5,11.4477153 8.94771525,11 9.5,11 L9.5,10.5 C9.5,9.11928813 10.6192881,8 12,8 C13.3807119,8 14.5,9.11928813 14.5,10.5 L14.5,11 Z M12,9 C11.1715729,9 10.5,9.67157288 10.5,10.5 L10.5,11 L13.5,11 L13.5,10.5 C13.5,9.67157288 12.8284271,9 12,9 Z" fill="#000000" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New order has been received.
																	</div>
																	<div class="kt-notification__item-time">
																		2 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z" fill="#000000" opacity="0.3" />
																			<path d="M13,5.06189375 C12.6724058,5.02104333 12.3386603,5 12,5 C11.6613397,5 11.3275942,5.02104333 11,5.06189375 L11,4 L10,4 C9.44771525,4 9,3.55228475 9,3 C9,2.44771525 9.44771525,2 10,2 L14,2 C14.5522847,2 15,2.44771525 15,3 C15,3.55228475 14.5522847,4 14,4 L13,4 L13,5.06189375 Z" fill="#000000" />
																			<path d="M16.7099142,6.53272645 L17.5355339,5.70710678 C17.9260582,5.31658249 18.5592232,5.31658249 18.9497475,5.70710678 C19.3402718,6.09763107 19.3402718,6.73079605 18.9497475,7.12132034 L18.1671361,7.90393167 C17.7407802,7.38854954 17.251061,6.92750259 16.7099142,6.53272645 Z" fill="#000000" />
																			<path d="M11.9630156,7.5 L12.0369844,7.5 C12.2982526,7.5 12.5154733,7.70115317 12.5355117,7.96165175 L12.9585886,13.4616518 C12.9797677,13.7369807 12.7737386,13.9773481 12.4984096,13.9985272 C12.4856504,13.9995087 12.4728582,14 12.4600614,14 L11.5399386,14 C11.2637963,14 11.0399386,13.7761424 11.0399386,13.5 C11.0399386,13.4872031 11.0404299,13.4744109 11.0414114,13.4616518 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer is registered
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<rect fill="#000000" opacity="0.3" x="2" y="9" width="20" height="13" rx="2" />
																			<rect fill="#000000" opacity="0.3" x="5" y="5" width="14" height="2" rx="0.5" />
																			<rect fill="#000000" opacity="0.3" x="7" y="1" width="10" height="2" rx="0.5" />
																			<path d="M10.8333333,20 C9.82081129,20 9,19.3159906 9,18.4722222 C9,17.6284539 9.82081129,16.9444444 10.8333333,16.9444444 C11.0476105,16.9444444 11.2533018,16.9750785 11.4444444,17.0313779 L11.4444444,12.7916011 C11.4444444,12.4782408 11.6398662,12.2012404 11.9268804,12.1077729 L15.4407693,11.0331119 C15.8834716,10.8889438 16.3333333,11.2336005 16.3333333,11.7169402 L16.3333333,12.7916011 C16.3333333,13.1498215 15.9979332,13.3786009 15.7222222,13.4444444 C15.3255297,13.53918 14.3070112,13.7428837 12.6666667,14.0555556 L12.6666667,18.5035214 C12.6666667,18.5583862 12.6622174,18.6091837 12.6535404,18.6559869 C12.5446237,19.4131089 11.771224,20 10.8333333,20 Z" fill="#000000" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Application has been approved
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect opacity="0.200000003" x="0" y="0" width="24" height="24" />
																			<path d="M4.5,7 L9.5,7 C10.3284271,7 11,7.67157288 11,8.5 C11,9.32842712 10.3284271,10 9.5,10 L4.5,10 C3.67157288,10 3,9.32842712 3,8.5 C3,7.67157288 3.67157288,7 4.5,7 Z M13.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L13.5,18 C12.6715729,18 12,17.3284271 12,16.5 C12,15.6715729 12.6715729,15 13.5,15 Z" fill="#000000" opacity="0.3" />
																			<path d="M17,11 C15.3431458,11 14,9.65685425 14,8 C14,6.34314575 15.3431458,5 17,5 C18.6568542,5 20,6.34314575 20,8 C20,9.65685425 18.6568542,11 17,11 Z M6,19 C4.34314575,19 3,17.6568542 3,16 C3,14.3431458 4.34314575,13 6,13 C7.65685425,13 9,14.3431458 9,16 C9,17.6568542 7.65685425,19 6,19 Z" fill="#000000" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer comment recieved
																	</div>
																	<div class="kt-notification__item-time">
																		2 days ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000" />
																			<rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
																		</g>
																	</svg> </div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer is registered
																	</div>
																	<div class="kt-notification__item-time">
																		3 days ago
																	</div>
																</div>
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>

										<!--end:: Widgets/Notifications-->
									</div>
								</div>
							</div>

							<!-- end:: Content -->
						</div>
					</div>

					<!-- begin:: Footer -->
					<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer" style="background-image: url('assets/media/bg/bg-2.jpg');">
						<div class="kt-footer__top">
							<div class="kt-container ">
								<div class="row">
									<div class="col-lg-4">
										<div class="kt-footer__section">
											<h3 class="kt-footer__title">About</h3>
											<div class="kt-footer__content">
												Lorem Ipsum is simply dummy text of the printing<br>
												and typesetting and typesetting industry has been the <br>
												industry's standard dummy text ever since the 1500s,<br>
												when an unknown printer took a galley of type.
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="kt-footer__section">
											<h3 class="kt-footer__title">Quick Links</h3>
											<div class="kt-footer__content">
												<div class="kt-footer__nav">
													<div class="kt-footer__nav-section">
														<a href="#">General Reports</a>
														<a href="#">Dashboart Widgets</a>
														<a href="#">Custom Pages</a>
													</div>
													<div class="kt-footer__nav-section">
														<a href="#">User Setting</a>
														<a href="#">Custom Pages</a>
														<a href="#">Intranet Settings</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="kt-footer__section">
											<h3 class="kt-footer__title">Get In Touch</h3>
											<div class="kt-footer__content">
												<form action="" class="kt-footer__subscribe">
													<div class="input-group">
														<input type="text" class="form-control" placeholder="Enter Your Email">
														<div class="input-group-append">
															<button class="btn btn-brand" type="button">Join</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-footer__bottom">
							<div class="kt-container ">
								<div class="kt-footer__wrapper">
									<div class="kt-footer__logo">
										<a class="kt-header__brand-logo" href="?page=index&amp;demo=demo2">
											<img alt="Logo" src="assets/media/logos/logo-4-sm.png" class="kt-header__brand-logo-sticky">
										</a>
										<div class="kt-footer__copyright">
											2019&nbsp;&copy;&nbsp;
											<a href="http://keenthemes.com/metronic" target="_blank">Keenthemes</a>
										</div>
									</div>
									<div class="kt-footer__menu">
										<a href="http://keenthemes.com/metronic" target="_blank">Purchase Lisence</a>
										<a href="http://keenthemes.com/metronic" target="_blank">Team</a>
										<a href="http://keenthemes.com/metronic" target="_blank">Contact</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- end:: Footer -->
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Quick Panel -->
		<div id="kt_quick_panel" class="kt-quick-panel">
			<a href="#" class="kt-quick-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></a>
			<div class="kt-quick-panel__nav">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">
					<li class="nav-item active">
						<a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">Notifications</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_logs" role="tab">Audit Logs</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Settings</a>
					</li>
				</ul>
			</div>
			<div class="kt-quick-panel__content">
				<div class="tab-content">
					<div class="tab-pane fade show kt-scroll active" id="kt_quick_panel_tab_notifications" role="tabpanel">
						<div class="kt-notification">
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-line-chart kt-font-success"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										New order has been received
									</div>
									<div class="kt-notification__item-time">
										2 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-box-1 kt-font-brand"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										New customer is registered
									</div>
									<div class="kt-notification__item-time">
										3 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-chart2 kt-font-danger"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										Application has been approved
									</div>
									<div class="kt-notification__item-time">
										3 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-image-file kt-font-warning"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										New file has been uploaded
									</div>
									<div class="kt-notification__item-time">
										5 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-drop kt-font-info"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										New user feedback received
									</div>
									<div class="kt-notification__item-time">
										8 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-pie-chart-2 kt-font-success"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										System reboot has been successfully completed
									</div>
									<div class="kt-notification__item-time">
										12 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-favourite kt-font-danger"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										New order has been placed
									</div>
									<div class="kt-notification__item-time">
										15 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item kt-notification__item--read">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-safe kt-font-primary"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										Company meeting canceled
									</div>
									<div class="kt-notification__item-time">
										19 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-psd kt-font-success"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										New report has been received
									</div>
									<div class="kt-notification__item-time">
										23 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon-download-1 kt-font-danger"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										Finance report has been generated
									</div>
									<div class="kt-notification__item-time">
										25 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon-security kt-font-warning"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										New customer comment recieved
									</div>
									<div class="kt-notification__item-time">
										2 days ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification__item">
								<div class="kt-notification__item-icon">
									<i class="flaticon2-pie-chart kt-font-warning"></i>
								</div>
								<div class="kt-notification__item-details">
									<div class="kt-notification__item-title">
										New customer is registered
									</div>
									<div class="kt-notification__item-time">
										3 days ago
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="tab-pane fade kt-scroll" id="kt_quick_panel_tab_logs" role="tabpanel">
						<div class="kt-notification-v2">
							<a href="#" class="kt-notification-v2__item">
								<div class="kt-notification-v2__item-icon">
									<i class="flaticon-bell kt-font-brand"></i>
								</div>
								<div class="kt-notification-v2__itek-wrapper">
									<div class="kt-notification-v2__item-title">
										5 new user generated report
									</div>
									<div class="kt-notification-v2__item-desc">
										Reports based on sales
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification-v2__item">
								<div class="kt-notification-v2__item-icon">
									<i class="flaticon2-box kt-font-danger"></i>
								</div>
								<div class="kt-notification-v2__itek-wrapper">
									<div class="kt-notification-v2__item-title">
										2 new items submited
									</div>
									<div class="kt-notification-v2__item-desc">
										by Grog John
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification-v2__item">
								<div class="kt-notification-v2__item-icon">
									<i class="flaticon-psd kt-font-brand"></i>
								</div>
								<div class="kt-notification-v2__itek-wrapper">
									<div class="kt-notification-v2__item-title">
										79 PSD files generated
									</div>
									<div class="kt-notification-v2__item-desc">
										Reports based on sales
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification-v2__item">
								<div class="kt-notification-v2__item-icon">
									<i class="flaticon2-supermarket kt-font-warning"></i>
								</div>
								<div class="kt-notification-v2__itek-wrapper">
									<div class="kt-notification-v2__item-title">
										$2900 worth producucts sold
									</div>
									<div class="kt-notification-v2__item-desc">
										Total 234 items
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification-v2__item">
								<div class="kt-notification-v2__item-icon">
									<i class="flaticon-paper-plane-1 kt-font-success"></i>
								</div>
								<div class="kt-notification-v2__itek-wrapper">
									<div class="kt-notification-v2__item-title">
										4.5h-avarage response time
									</div>
									<div class="kt-notification-v2__item-desc">
										Fostest is Barry
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification-v2__item">
								<div class="kt-notification-v2__item-icon">
									<i class="flaticon2-information kt-font-danger"></i>
								</div>
								<div class="kt-notification-v2__itek-wrapper">
									<div class="kt-notification-v2__item-title">
										Database server is down
									</div>
									<div class="kt-notification-v2__item-desc">
										10 mins ago
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification-v2__item">
								<div class="kt-notification-v2__item-icon">
									<i class="flaticon2-mail-1 kt-font-brand"></i>
								</div>
								<div class="kt-notification-v2__itek-wrapper">
									<div class="kt-notification-v2__item-title">
										System report has been generated
									</div>
									<div class="kt-notification-v2__item-desc">
										Fostest is Barry
									</div>
								</div>
							</a>
							<a href="#" class="kt-notification-v2__item">
								<div class="kt-notification-v2__item-icon">
									<i class="flaticon2-hangouts-logo kt-font-warning"></i>
								</div>
								<div class="kt-notification-v2__itek-wrapper">
									<div class="kt-notification-v2__item-title">
										4.5h-avarage response time
									</div>
									<div class="kt-notification-v2__item-desc">
										Fostest is Barry
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="tab-pane kt-quick-panel__content-padding-x fade kt-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">
						<form class="kt-form">
							<div class="kt-heading kt-heading--sm kt-heading--space-sm">Customer Care</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable Notifications:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--success kt-switch--sm">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_1">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable Case Tracking:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--success kt-switch--sm">
										<label>
											<input type="checkbox" name="quick_panel_notifications_2">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Support Portal:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--success kt-switch--sm">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_2">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>
							<div class="kt-heading kt-heading--sm kt-heading--space-sm">Reports</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Generate Reports:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_3">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable Report Export:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox" name="quick_panel_notifications_3">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Allow Data Collection:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_4">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>
							<div class="kt-heading kt-heading--sm kt-heading--space-sm">Memebers</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable Member singup:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--brand">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_5">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Allow User Feedbacks:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--brand">
										<label>
											<input type="checkbox" name="quick_panel_notifications_5">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Enable Customer Portal:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--brand">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_6">
											<span></span>
										</label>
									</span>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- end::Quick Panel -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

		<!-- end::Scrolltop -->

		<!-- begin::Sticky Toolbar -->
		<ul class="kt-sticky-toolbar" style="margin-top: 30px;">
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--success" id="kt_demo_panel_toggle" data-toggle="kt-tooltip" title="Check out more demos" data-placement="right">
				<a href="#" class=""><i class="flaticon2-drop"></i></a>
			</li>
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--brand" data-toggle="kt-tooltip" title="Layout Builder" data-placement="left">
				<a href="https://keenthemes.com/metronic/preview/demo4/builder.html" target="_blank"><i class="flaticon2-gear"></i></a>
			</li>
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--warning" data-toggle="kt-tooltip" title="Documentation" data-placement="left">
				<a href="https://keenthemes.com/metronic/?page=docs" target="_blank"><i class="flaticon2-telegram-logo"></i></a>
			</li>
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--danger" id="kt_sticky_toolbar_chat_toggler" data-toggle="kt-tooltip" title="Chat Example" data-placement="left">
				<a href="#" data-toggle="modal" data-target="#kt_chat_modal"><i class="flaticon2-chat-1"></i></a>
			</li>
		</ul>

		<!-- end::Sticky Toolbar -->

		<!-- begin::Demo Panel -->
		<div id="kt_demo_panel" class="kt-demo-panel">
			<div class="kt-demo-panel__head">
				<h3 class="kt-demo-panel__title">
					Select A Demo

					<!--<small>5</small>-->
				</h3>
				<a href="#" class="kt-demo-panel__close" id="kt_demo_panel_close"><i class="flaticon2-delete"></i></a>
			</div>
			<div class="kt-demo-panel__body">
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 1
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo1.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo1/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo1/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 2
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo2.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo2/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo2/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 3
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo3.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo3/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo3/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item kt-demo-panel__item--active">
					<div class="kt-demo-panel__item-title">
						Demo 4
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo4.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo4/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo4/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 5
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo5.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo5/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo5/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 6
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo6.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo6/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo6/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 7
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo7.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo7/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo7/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 8
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo8.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo8/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo8/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 9
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo9.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo9/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo9/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 10
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo10.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo10/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo10/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 11
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo11.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo11/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo11/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 12
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo12.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="https://keenthemes.com/metronic/preview/demo12/" class="btn btn-brand btn-elevate " target="_blank">Default</a>
							<a href="https://keenthemes.com/metronic/preview/demo12/rtl/" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 13
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo13.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
						</div>
					</div>
				</div>
				<div class="kt-demo-panel__item ">
					<div class="kt-demo-panel__item-title">
						Demo 14
					</div>
					<div class="kt-demo-panel__item-preview">
						<img src="assets/media//demos/preview/demo14.jpg" alt="" />
						<div class="kt-demo-panel__item-preview-overlay">
							<a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
						</div>
					</div>
				</div>
				<a href="https://1.envato.market/EA4JP" target="_blank" class="kt-demo-panel__purchase btn btn-brand btn-elevate btn-bold btn-upper">
					Buy Metronic Now!
				</a>
			</div>
		</div>

		<!-- end::Demo Panel -->

		<!--Begin:: Chat-->
		<div class="modal fade- modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="kt-chat">
						<div class="kt-portlet kt-portlet--last">
							<div class="kt-portlet__head">
								<div class="kt-chat__head ">
									<div class="kt-chat__left">
										<div class="kt-chat__label">
											<a href="#" class="kt-chat__title">Jason Muller</a>
											<span class="kt-chat__status">
												<span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
											</span>
										</div>
									</div>
									<div class="kt-chat__right">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-clean btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="flaticon-more-1"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-md">

												<!--begin::Nav-->
												<ul class="kt-nav">
													<li class="kt-nav__head">
														Messaging
														<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
													</li>
													<li class="kt-nav__separator"></li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-group"></i>
															<span class="kt-nav__link-text">New Group</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-open-text-book"></i>
															<span class="kt-nav__link-text">Contacts</span>
															<span class="kt-nav__link-badge">
																<span class="kt-badge kt-badge--brand  kt-badge--rounded-">5</span>
															</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-bell-2"></i>
															<span class="kt-nav__link-text">Calls</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-dashboard"></i>
															<span class="kt-nav__link-text">Settings</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-protected"></i>
															<span class="kt-nav__link-text">Help</span>
														</a>
													</li>
													<li class="kt-nav__separator"></li>
													<li class="kt-nav__foot">
														<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
														<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
													</li>
												</ul>

												<!--end::Nav-->
											</div>
										</div>
										<button type="button" class="btn btn-clean btn-sm btn-icon" data-dismiss="modal">
											<i class="flaticon2-cross"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-scroll kt-scroll--pull" data-height="410" data-mobile-height="300">
									<div class="kt-chat__messages kt-chat__messages--solid">
										<div class="kt-chat__message kt-chat__message--success">
											<div class="kt-chat__user">
												<span class="kt-media kt-media--circle kt-media--sm">
													<img src="assets/media/users/100_12.jpg" alt="image">
												</span>
												<a href="#" class="kt-chat__username">Jason Muller</span></a>
												<span class="kt-chat__datetime">2 Hours</span>
											</div>
											<div class="kt-chat__text">
												How likely are you to recommend our company<br> to your friends and family?
											</div>
										</div>
										<div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
											<div class="kt-chat__user">
												<span class="kt-chat__datetime">30 Seconds</span>
												<a href="#" class="kt-chat__username">You</span></a>
												<span class="kt-media kt-media--circle kt-media--sm">
													<img src="assets/media/users/300_21.jpg" alt="image">
												</span>
											</div>
											<div class="kt-chat__text">
												Hey there, we’re just writing to let you know that you’ve<br> been subscribed to a repository on GitHub.
											</div>
										</div>
										<div class="kt-chat__message kt-chat__message--success">
											<div class="kt-chat__user">
												<span class="kt-media kt-media--circle kt-media--sm">
													<img src="assets/media/users/100_12.jpg" alt="image">
												</span>
												<a href="#" class="kt-chat__username">Jason Muller</span></a>
												<span class="kt-chat__datetime">30 Seconds</span>
											</div>
											<div class="kt-chat__text">
												Ok, Understood!
											</div>
										</div>
										<div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
											<div class="kt-chat__user">
												<span class="kt-chat__datetime">Just Now</span>
												<a href="#" class="kt-chat__username">You</span></a>
												<span class="kt-media kt-media--circle kt-media--sm">
													<img src="assets/media/users/300_21.jpg" alt="image">
												</span>
											</div>
											<div class="kt-chat__text">
												You’ll receive notifications for all issues, pull requests!
											</div>
										</div>
										<div class="kt-chat__message kt-chat__message--success">
											<div class="kt-chat__user">
												<span class="kt-media kt-media--circle kt-media--sm">
													<img src="assets/media/users/100_12.jpg" alt="image">
												</span>
												<a href="#" class="kt-chat__username">Jason Muller</span></a>
												<span class="kt-chat__datetime">2 Hours</span>
											</div>
											<div class="kt-chat__text">
												You were automatically <b class="kt-font-brand">subscribed</b> <br>because you’ve been given access to the repository
											</div>
										</div>
										<div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
											<div class="kt-chat__user">
												<span class="kt-chat__datetime">30 Seconds</span>
												<a href="#" class="kt-chat__username">You</span></a>
												<span class="kt-media kt-media--circle kt-media--sm">
													<img src="assets/media/users/300_21.jpg" alt="image">
												</span>
											</div>
											<div class="kt-chat__text">
												You can unwatch this repository immediately <br>by clicking here: <a href="#" class="kt-font-bold kt-link"></a>
											</div>
										</div>
										<div class="kt-chat__message kt-chat__message--success">
											<div class="kt-chat__user">
												<span class="kt-media kt-media--circle kt-media--sm">
													<img src="assets/media/users/100_12.jpg" alt="image">
												</span>
												<a href="#" class="kt-chat__username">Jason Muller</span></a>
												<span class="kt-chat__datetime">30 Seconds</span>
											</div>
											<div class="kt-chat__text">
												Discover what students who viewed Learn <br>Figma - UI/UX Design Essential Training also viewed
											</div>
										</div>
										<div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
											<div class="kt-chat__user">
												<span class="kt-chat__datetime">Just Now</span>
												<a href="#" class="kt-chat__username">You</span></a>
												<span class="kt-media kt-media--circle kt-media--sm">
													<img src="assets/media/users/300_21.jpg" alt="image">
												</span>
											</div>
											<div class="kt-chat__text">
												Most purchased Business courses during this sale!
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-chat__input">
									<div class="kt-chat__editor">
										<textarea placeholder="Type here..." style="height: 50px"></textarea>
									</div>
									<div class="kt-chat__toolbar">
										<div class="kt_chat__tools">
											<a href="#"><i class="flaticon2-link"></i></a>
											<a href="#"><i class="flaticon2-photograph"></i></a>
											<a href="#"><i class="flaticon2-photo-camera"></i></a>
										</div>
										<div class="kt_chat__actions">
											<button type="button" class="btn btn-brand btn-md  btn-font-sm btn-upper btn-bold kt-chat__reply">reply</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--ENd:: Chat-->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#366cf3",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->

		<!--begin:: Vendor Plugins -->
		<script src="assets/plugins/general/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="assets/plugins/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="assets/plugins/general/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="assets/plugins/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/wnumb/wNumb.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="assets/plugins/general/plugins/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="assets/plugins/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="assets/plugins/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="assets/plugins/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="assets/plugins/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="assets/plugins/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="assets/plugins/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="assets/plugins/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="assets/plugins/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="assets/plugins/general/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="assets/plugins/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/dropzone.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/quill/dist/quill.js" type="text/javascript"></script>
		<script src="assets/plugins/general/@yaireo/tagify/dist/tagify.polyfills.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/@yaireo/tagify/dist/tagify.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="assets/plugins/general/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/jquery-validation.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/dual-listbox/dist/dual-listbox.js" type="text/javascript"></script>
		<script src="assets/plugins/general/raphael/raphael.js" type="text/javascript"></script>
		<script src="assets/plugins/general/morris.js/morris.js" type="text/javascript"></script>
		<script src="assets/plugins/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="assets/plugins/general/plugins/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/plugins/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="assets/plugins/general/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="assets/plugins/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/sweetalert2.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="assets/plugins/general/dompurify/dist/purify.js" type="text/javascript"></script>

		<!--end:: Vendor Plugins -->
		<script src="assets/js/scripts.bundle.js" type="text/javascript"></script>

		<!--begin:: Vendor Plugins for custom pages -->
		<script src="assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/core/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/daygrid/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/google-calendar/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/interaction/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/list/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/timegrid/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/dist/es5/jquery.flot.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.resize.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.categories.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.pie.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.stack.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.crosshair.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.axislabels.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/js/global/integration/plugins/datatables.init.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jszip/dist/jszip.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/pdfmake/build/pdfmake.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/pdfmake/build/vfs_fonts.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/buttons.colVis.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/buttons.flash.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/buttons.html5.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/buttons.print.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-select/js/dataTables.select.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/jquery.vmap.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.world.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.russia.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.usa.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.germany.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.europe.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/uppy/dist/uppy.min.js" type="text/javascript"></script>

		<!--end:: Vendor Plugins for custom pages -->

		<!--end::Global Theme Bundle -->

		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/dashboard.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
