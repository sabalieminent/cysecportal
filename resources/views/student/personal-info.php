<?php include("header.blade.php") ?>

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">

								<!--Begin::App-->
								<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

									<!--Begin:: App Aside Mobile Toggle-->
									<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
										<i class="la la-close"></i>
									</button>

									<!--End:: App Aside Mobile Toggle-->

									<!--Begin:: App Aside-->
									<div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

										<?php include ("side-menu.php")?>
									</div>

									<!--End:: App Aside-->

									<!--Begin:: App Content-->
									<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
										<div class="row">
											<div class="col-xl-12">
												<div class="kt-portlet kt-portlet--height-fluid">
													<div class="kt-portlet__head">
														<div class="kt-portlet__head-label">
															<h3 class="kt-portlet__head-title">Personal Information</h3>
														</div>
													</div>
													<form class="kt-form kt-form--label-right">
														<div class="kt-portlet__body">


                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                <tr>
                                                                <td style="width: 30%; font-weight: bold" >Title</td>
                                                                <td> Pastor  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold" >Suffix</td>
                                                                    <td> Pastor  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold" >Full Name</td>
                                                                    <td> Ositadimma Tochukwu Michael
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Email</td>
                                                                    <td>jsdkjdksj@sdsk.com</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Mobile Contact</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Alt Mobile Contact</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Marital Status</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Gender</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Home Country</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Home State</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Home Address</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Residential Country</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Residential State</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Residential Address</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">Postal Address</td>
                                                                    <td>898394893849</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 30%; font-weight: bold">About Me</td>
                                                                    <td>898394893849</td>
                                                                </tr>

                                                                </tbody></table>


														</div>
														<div class="kt-portlet__foot">
															<div class="kt-form__actions">
																<div class="row">
																	<div class="col-lg-3 col-xl-3">
																	</div>
																	<div class="col-lg-9 col-xl-9">
                                                                        <a href="update-profile.php" button type="reset" class="btn btn-brand btn-bold">Edit Personal Info</button>&nbsp;</a>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

									<!--End:: App Content-->
								</div>

								<!--End::App-->
							</div>

							<!-- end:: Content -->
						</div>
					</div>

					<?php include ("footer.php") ?>
		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/dashboard.js" type="text/javascript"></script>
		<script src="assets/js/pages/custom/user/profile.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
