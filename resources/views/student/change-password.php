<?php include("header.blade.php") ?>

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">

								<!--Begin::App-->
								<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

									<!--Begin:: App Aside Mobile Toggle-->
									<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
										<i class="la la-close"></i>
									</button>

									<!--End:: App Aside Mobile Toggle-->

									<!--Begin:: App Aside-->
									<div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

										<?php include "side-menu.php";?>
									</div>

									<!--End:: App Aside-->

									<!--Begin:: App Content-->
									<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
										<div class="row">
											<div class="col-xl-12">
												<div class="kt-portlet kt-portlet--height-fluid">
													<div class="kt-portlet__head">
														<div class="kt-portlet__head-label">
															<h3 class="kt-portlet__head-title">Change Password<small>change or reset your account password</small></h3>
														</div>

													</div>
													<form class="kt-form kt-form--label-right">
														<div class="kt-portlet__body">
															<div class="kt-section kt-section--first">
																<div class="kt-section__body">
																	<div class="alert alert-solid-danger alert-bold fade show kt-margin-t-20 kt-margin-b-40" role="alert">
																		<div class="alert-icon"><i class="fa fa-exclamation-triangle"></i></div>
																		<div class="alert-text">Configure user passwords to expire periodically. Users will need warning that their passwords are going to expire, <br>or they might inadvertently get locked out of the system!</div>
																		<div class="alert-close">
																			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																				<span aria-hidden="true"><i class="la la-close"></i></span>
																			</button>
																		</div>
																	</div>
																	<div class="row">
																		<label class="col-xl-3"></label>
																		<div class="col-lg-9 col-xl-6">
																			<h3 class="kt-section__title kt-section__title-sm">Change Your Password:</h3>
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-xl-3 col-lg-3 col-form-label">Current Password</label>
																		<div class="col-lg-9 col-xl-6">
																			<input type="password" class="form-control" value="" placeholder="Current password">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-xl-3 col-lg-3 col-form-label">New Password</label>
																		<div class="col-lg-9 col-xl-6">
																			<input type="password" class="form-control" value="" placeholder="New password">
																		</div>
																	</div>
																	<div class="form-group form-group-last row">
																		<label class="col-xl-3 col-lg-3 col-form-label">Verify Password</label>
																		<div class="col-lg-9 col-xl-6">
																			<input type="password" class="form-control" value="" placeholder="Verify password">
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="kt-portlet__foot">
															<div class="kt-form__actions">
																<div class="row">
																	<div class="col-lg-3 col-xl-3">
																	</div>
																	<div class="col-lg-9 col-xl-9">
																		<button type="reset" class="btn btn-brand btn-bold">Change Password</button>&nbsp;
																		<button type="reset" class="btn btn-secondary">Cancel</button>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

									<!--End:: App Content-->
								</div>

								<!--End::App-->
							</div>

							<!-- end:: Content -->
						</div>
					</div>

					<?php include ("footer.php") ?>

		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/dashboard.js" type="text/javascript"></script>
		<script src="assets/js/pages/custom/user/profile.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
