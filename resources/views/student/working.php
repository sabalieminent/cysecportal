<?php include("header.blade.php") ?>

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">

								<!--Begin::App-->
								<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

									<!--Begin:: App Aside Mobile Toggle-->
									<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
										<i class="la la-close"></i>
									</button>

									<!--End:: App Aside Mobile Toggle-->



									<!--Begin:: App Content-->
									<div class="kt-grid__item kt-grid__item--fluid kt-app__content" >
										<div class="row">
											<div class="col-xl-12" >
												<div class="kt-portlet kt-portlet--height-fluid">
													<div class="kt-portlet__head">
														<div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title"><strong>Educational Qualification</strong></h3>
														</div>

													</div>
													<form class="kt-form kt-form--label-right">
														<div class="kt-portlet__body">
															<div class="kt-section kt-section--first">
																<div class="kt-section__body">


																	<div class="form-group row">
																		<label class="col-xl-3 col-lg-3 col-form-label">Institution Name</label>
																		<div class="col-lg-9 col-xl-6">
																			<input type="text" class="form-control" value="" placeholder="">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-xl-3 col-lg-3 col-form-label">Graduation Year</label>
																		<div class="col-lg-9 col-xl-6">
																			<input type="text" class="form-control" value="" placeholder="">
																		</div>
																	</div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Qualification</label>
                                                                        <div class="col-lg-9 col-xl-6">
                                                                            <select name="country" class="form-control" aria-describedby="country-error" aria-invalid="false">
                                                                                <option value="">Select</option>
                                                                                <option value="AF">Afghanistan</option>

                                                                            </select>                                                                        </div>
                                                                    </div>
																	<div class="form-group form-group-last row">
																		<label class="col-xl-3 col-lg-3 col-form-label">Educational Level</label>
																		<div class="col-lg-9 col-xl-6">
                                                                            <select name="country" class="form-control" aria-describedby="country-error" aria-invalid="false">
                                                                                <option value="">Select</option>
                                                                                <option value="AF">Afghanistan</option>

                                                                            </select>																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="kt-portlet__foot">
															<div class="kt-form__actions">
																<div class="row">
																	<div class="col-lg-3 col-xl-3">
																	</div>
																	<div class="col-lg-9 col-xl-9">
																		<button type="submit" id="kt_sweetalert_demo_3_3" class="btn btn-brand btn-bold">Submit</button>&nbsp;
																		<button type="reset" class="btn btn-secondary">Cancel</button>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

									<!--End:: App Content-->
								</div>

								<!--End::App-->
							</div>

							<!-- end:: Content -->
						</div>
					</div>

					<?php include ("footer.php") ?>

		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/dashboard.js" type="text/javascript"></script>
		<script src="assets/js/pages/custom/user/profile.js" type="text/javascript"></script>
<script src="assets/js/pages/components/extended/sweetalert2.js" type="text/javascript"></script>
		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
