<?php include("header.blade.php") ?>

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">

								<!--Begin::App-->
								<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

									<!--Begin:: App Aside Mobile Toggle-->
									<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
										<i class="la la-close"></i>
									</button>

									<!--End:: App Aside Mobile Toggle-->

									<!--Begin:: App Aside-->
									<div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

										<?php include ("side-menu.php")?>
									</div>

									<!--End:: App Aside-->

									<!--Begin:: App Content-->
                                    <div class="kt-portlet" style="margin-left: 10px">
                                        <div class="kt-portlet__head kt-portlet__head--lg">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Professional Qualification
                                                </h3>


                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                            <div class="kt-portlet__head-wrapper">
                                                <div class="kt-portlet__head-actions">

                                                    <a href="professional.php" class="btn btn-brand btn-elevate btn-icon-sm">
                                                        <i class="la la-plus"></i>
                                                        Add New Record
                                                    </a>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">

                                            <!--begin::Section-->
                                            <div class="kt-section">
                                                <div class="kt-section__content">
                                                    <table class="table table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th style="font-weight: bold">#</th>
                                                            <th style="font-weight: bold">Institution </th>
                                                            <th style="font-weight: bold">Certification</th>
                                                            <th style="font-weight: bold">Certification No</th>
                                                            <th style="font-weight: bold">Year Issued</th>
                                                            <th style="font-weight: bold">Year Expiring</th>
                                                            <th style="font-weight: bold">Edit</th>
                                                            <th style="font-weight: bold">Delete</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <th scope="row">1</th>
                                                            <td>University of Nigeria Nsukka</td>
                                                            <td>2020</td>
                                                            <td>B.Sc</td>
                                                            <td>SDff</td>
                                                            <td>SDff</td>
                                                            <td>
                                                                <button class="btn btn-success fa fa-edit" data-toggle="modal" data-target="#kt_modal_4"> Edit </button>
                                                            </td>
                                                            <td>
                                                                <button id="kt_sweetalert_demo_9" type="button" class="btn btn-danger la la-trash " id="#"> Delete </button>
                                                            </td>
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <!--end::Section-->
                                        </div>

                                        <!--end::Form-->
                                    </div>
									<!--End:: App Content-->
								</div></div>

								<!--End::App-->
							</div>
</div>
<div class="modal fade" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Your Professional Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Institution Name:</label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Certification Name:</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Year Issued:</label>
                        <input class="form-control" type="text" required="required">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Expiring Year: <code>if not expiring input "NONE"</code></label>
                        <input class="form-control" type="text" required="required">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-success" id="kt_sweetalert_demo_3_3">Update</button>
            </div>
        </div>
    </div>
</div>
							<!-- end:: Content -->
						</div>
					</div>

					<?php include ("footer.php") ?>

		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/dashboard.js" type="text/javascript"></script>
		<script src="assets/js/pages/custom/user/profile.js" type="text/javascript"></script>
<script src="assets/js/pages/components/extended/sweetalert2.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
