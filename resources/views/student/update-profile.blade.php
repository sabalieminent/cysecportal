<?php include("header.blade.php") ?>
<link href="assets/css/pages/wizard/wizard-4.css" rel="stylesheet" type="text/css" />

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">

									<!--begin: Form Wizard Nav -->
									<div class="kt-wizard-v4__nav">
										<div class="kt-wizard-v4__nav-items nav">

											<!--doc: Replace A tag with SPAN tag to disable the step link click -->
											<div class="kt-wizard-v4__nav-item nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
												<div class="kt-wizard-v4__nav-body">
													<div class="kt-wizard-v4__nav-number">
														1
													</div>
													<div class="kt-wizard-v4__nav-label">
														<div class="kt-wizard-v4__nav-label-title">
															Profile
														</div>
														<div class="kt-wizard-v4__nav-label-desc">
															User's Personal Information
														</div>
													</div>
												</div>
											</div>
											<div class="kt-wizard-v4__nav-item nav-item" data-ktwizard-type="step">
												<div class="kt-wizard-v4__nav-body">
													<div class="kt-wizard-v4__nav-number">
														2
													</div>
													<div class="kt-wizard-v4__nav-label">
														<div class="kt-wizard-v4__nav-label-title">
															Account
														</div>
														<div class="kt-wizard-v4__nav-label-desc">
															User's Account & Settings
														</div>
													</div>
												</div>
											</div>
											<div class="kt-wizard-v4__nav-item nav-item" data-ktwizard-type="step">
												<div class="kt-wizard-v4__nav-body">
													<div class="kt-wizard-v4__nav-number">
														3
													</div>
													<div class="kt-wizard-v4__nav-label">
														<div class="kt-wizard-v4__nav-label-title">
															Address
														</div>
														<div class="kt-wizard-v4__nav-label-desc">
															User's Shipping Address
														</div>
													</div>
												</div>
											</div>
											<div class="kt-wizard-v4__nav-item nav-item" data-ktwizard-type="step">
												<div class="kt-wizard-v4__nav-body">
													<div class="kt-wizard-v4__nav-number">
														4
													</div>
													<div class="kt-wizard-v4__nav-label">
														<div class="kt-wizard-v4__nav-label-title">
															Submission
														</div>
														<div class="kt-wizard-v4__nav-label-desc">
															Review and Submit
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--end: Form Wizard Nav -->
									<div class="kt-portlet">
										<div class="kt-portlet__body kt-portlet__body--fit">
											<div class="kt-grid">
												<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

													<!--begin: Form Wizard Form-->
													<form class="kt-form" id="kt_user_add_form">

														<!--begin: Form Wizard Step 1-->
														<div class="kt-wizard-v4__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
															<div class="kt-heading kt-heading--md">User's Profile Details:</div>
															<div class="kt-section kt-section--first">
																<div class="kt-wizard-v4__form">
																	<div class="row">
																		<div class="col-xl-12">
																			<div class="kt-section__body">
																				<div class="form-group row">
																					<label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
																					<div class="col-lg-9 col-xl-6">
																						<div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
																							<div class="kt-avatar__holder" style="background-image: url(assets/media/users/300_10.jpg)"></div>
																							<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Change avatar">
																								<i class="fa fa-pen"></i>
																								<input type="file" name="kt_user_add_user_avatar">
																							</label>
																							<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Cancel avatar">
																								<i class="fa fa-times"></i>
																							</span>
																						</div>
																					</div>
																				</div>
                                                                                <div class="row">
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Title</label>
                                                                                            <select class="form-control">
                                                                                                <option>Select Title...</option>
                                                                                                <option value="id">Bahasa Indonesia - Indonesian</option>
                                                                                                <option value="msa">Bahasa Melayu - Malay</option>
                                                                                                <option value="ca">Català - Catalan</option>

                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Suffix</label>
                                                                                            <select class="form-control">
                                                                                                <option>Select Suffix...</option>
                                                                                                <option value="id">Bahasa Indonesia - Indonesian</option>
                                                                                                <option value="msa">Bahasa Melayu - Malay</option>
                                                                                                <option value="ca">Català - Catalan</option>

                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>First Name</label>
                                                                                            <input type="text" class="form-control" name="postcode" placeholder="First Name" value="2000">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Middle Name</label>
                                                                                            <input type="text" class="form-control" name="state" placeholder="Middle Name" value="London">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Last Name</label>
                                                                                            <input type="text" class="form-control" name="postcode" placeholder="Postcode" value="2000">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Gender</label>
                                                                                            <select class="form-control">
                                                                                                <option>Select Gender...</option>
                                                                                                <option value="id">Bahasa Indonesia - Indonesian</option>
                                                                                                <option value="msa">Bahasa Melayu - Malay</option>
                                                                                                <option value="ca">Català - Catalan</option>

                                                                                            </select>

                                                                                             </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Mobile Contact</label>
                                                                                            <input type="text" class="form-control" value="+23478967456" placeholder="Phone" aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Alternate Mobile Contact</label>
                                                                                            <input type="text" class="form-control" value="+23478967456" placeholder="Phone" aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group-last row">
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Alternate Email</label>
                                                                                            <input type="email" class="form-control" name="postcode" placeholder="Postcode" value="info@cysec.com">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xl-6">
                                                                                        <div class="form-group">
                                                                                            <label>Alternate Email</label>
                                                                                            <input type="date" class="form-control" name="postcode" placeholder="Postcode" value="2000">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end: Form Wizard Step 1-->

														<!--begin: Form Wizard Step 2-->
														<div class="kt-wizard-v4__content" data-ktwizard-type="step-content">
															<div class="kt-section kt-section--first">
																<div class="kt-wizard-v4__form">
																	<div class="row">
																		<div class="col-xl-12">
																			<div class="kt-section__body">
																				<div class="form-group row">
																					<div class="col-lg-9 col-xl-6">
																						<h3 class="kt-section__title kt-section__title-md">User's Account Details</h3>
																					</div>
																				</div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Residential Country</label>
                                                                                    <div class="col-lg-9 col-xl-9">
                                                                                        <select class="form-control">
                                                                                            <option>Select Home Country...</option>
                                                                                            <option value="id">Bahasa Indonesia - Indonesian</option>
                                                                                            <option value="msa">Bahasa Melayu - Malay</option>
                                                                                            <option value="ca">Català - Catalan</option>

                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Residential State</label>
                                                                                    <div class="col-lg-9 col-xl-9">
                                                                                        <select class="form-control">
                                                                                            <option>Select State...</option>
                                                                                            <option value="id">Bahasa Indonesia - Indonesian</option>
                                                                                            <option value="msa">Bahasa Melayu - Malay</option>
                                                                                            <option value="ca">Català - Catalan</option>

                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Residential Address</label>
                                                                                    <div class="col-lg-9 col-xl-9">
                                                                                        <input class="form-control" type="text" value="Krox">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Home Country</label>
                                                                                    <div class="col-lg-9 col-xl-9">
                                                                                        <select class="form-control">
                                                                                            <option>Select Home Country...</option>
                                                                                            <option value="id">Bahasa Indonesia - Indonesian</option>
                                                                                            <option value="msa">Bahasa Melayu - Malay</option>
                                                                                            <option value="ca">Català - Catalan</option>

                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Home State</label>
                                                                                    <div class="col-lg-9 col-xl-9">
                                                                                        <select class="form-control">
                                                                                            <option>Select State...</option>
                                                                                            <option value="id">Bahasa Indonesia - Indonesian</option>
                                                                                            <option value="msa">Bahasa Melayu - Malay</option>
                                                                                            <option value="ca">Català - Catalan</option>

                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Home Address</label>
                                                                                    <div class="col-lg-9 col-xl-9">
                                                                                        <input class="form-control" type="text" value="Krox">
                                                                                    </div>
                                                                                </div>







																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end: Form Wizard Step 2-->

														<!--begin: Form Wizard Step 3-->
														<div class="kt-wizard-v4__content" data-ktwizard-type="step-content">
															<div class="kt-heading kt-heading--md">Setup Your Address</div>
															<div class="kt-form__section kt-form__section--first">
																<div class="kt-wizard-v4__form">
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Occupation</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <select class="form-control">
                                                                                <option>Select State...</option>
                                                                                <option value="id">Bahasa Indonesia - Indonesian</option>
                                                                                <option value="msa">Bahasa Melayu - Malay</option>
                                                                                <option value="ca">Català - Catalan</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>
																	<div class="form-group">
																		<label>About Me</label>
                                                                        <textarea rows="5" class="form-control" name="address1" placeholder="Address Line 1" value="Address Line 1"> </textarea>
																		<span class="form-text text-muted">Not More Than 1000 Words.</span>
																	</div>


																	<div class="row">

																	</div>
																</div>
															</div>
														</div>

														<!--end: Form Wizard Step 3-->

														<!--begin: Form Wizard Step 4-->
														<div class="kt-wizard-v4__content" data-ktwizard-type="step-content">
															<div class="kt-heading kt-heading--md">Review your Details and Submit</div>
															<div class="kt-form__section kt-form__section--first">
																<div class="kt-wizard-v4__review">
																	<div class="kt-wizard-v4__review-item">
																		<div class="kt-wizard-v4__review-title">
																			Your Account Details
																		</div>
																		<div class="kt-wizard-v4__review-content">
																			John Wick
																			<br /> Phone: +61412345678
																			<br /> Email: johnwick@reeves.com
																		</div>
																	</div>
																	<div class="kt-wizard-v4__review-item">
																		<div class="kt-wizard-v4__review-title">
																			Your Address Details
																		</div>
																		<div class="kt-wizard-v4__review-content">
																			Address Line 1
																			<br /> Address Line 2
																			<br /> Melbourne 3000, VIC, Australia
																		</div>
																	</div>
																	<div class="kt-wizard-v4__review-item">
																		<div class="kt-wizard-v4__review-title">
																			Payment Details
																		</div>
																		<div class="kt-wizard-v4__review-content">
																			Card Number: xxxx xxxx xxxx 1111
																			<br /> Card Name: John Wick
																			<br /> Card Expiry: 01/21
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end: Form Wizard Step 4-->

														<!--begin: Form Actions -->
														<div class="kt-form__actions">
															<div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
																Previous
															</div>
															<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
																Submit
															</div>
															<div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
																Next Step
															</div>
														</div>

														<!--end: Form Actions -->
													</form>

													<!--end: Form Wizard Form-->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- end:: Content -->
						</div>
					</div>

					<?php include ("footer.php") ?>
		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/custom/user/add-user.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
